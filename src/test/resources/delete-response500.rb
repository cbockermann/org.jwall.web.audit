list = $events.list( "RESPONSE_STATUS = 500" )

del = []

list.each {
  |e|
  id = e.get( "TX_ID" )
  puts "deleting event #{id}"
  del.push( "#{id}" )
}

del.each {
  |d|
  $events.delete( d )
}