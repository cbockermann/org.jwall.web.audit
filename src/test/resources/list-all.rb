#
# output all event IDs
#

list = $events.list()
list.each {
  |e|
  id = e.get( "UNIQUE_ID" )
  puts "#{id}"
}


#
# output event IDs for events with RESPONSE_STATUS = 500
#
list.each {
  |e|
  status = e.get( "RESPONSE_STATUS")
  if( status == "500" )
    id = e.get( "UNIQUE_ID" )
    puts "#{id}"
  end
}