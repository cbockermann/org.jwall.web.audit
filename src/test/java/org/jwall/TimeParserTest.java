/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall;


import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.jwall.log.io.TimeParser;

public class TimeParserTest {

	@Before
	public void setUp() throws Exception {
	}

	
	@Test
	public void testParseTime() throws Exception {
		String str = "1 hour";
		TimeParser p = new TimeParser();
		Long time = p.parse( str );
		Assert.assertEquals( TimeParser.HOUR, time );
	}
	
	@Test
	public void testParseHourShortened() throws Exception {
		String str = "2 hours";
		TimeParser p = new TimeParser();
		Long time = p.parse( str );
		Assert.assertEquals( 2*TimeParser.HOUR, time.longValue() );
	}
	
	@Test
	public void testParseMix() throws Exception {
		String str = "2 hours 34 minutes 1sec";
		TimeParser p = new TimeParser();
		Long time = p.parse( str );
		Assert.assertEquals( 2*TimeParser.HOUR + 34 * TimeParser.MINUTE + TimeParser.SECOND, time.longValue() );
	}


	
	@Test
	public void testParseMix2() throws Exception {
		String str = "2hours34minutes 1sec";
		TimeParser p = new TimeParser();
		Long time = p.parse( str );
		Assert.assertEquals( 2*TimeParser.HOUR + 34 * TimeParser.MINUTE + TimeParser.SECOND, time.longValue() );
	}
}
