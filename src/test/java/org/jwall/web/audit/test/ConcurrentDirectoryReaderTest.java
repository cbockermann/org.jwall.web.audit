/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.test;

import java.io.File;
import java.net.URL;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.io.ConcurrentDirectoryReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConcurrentDirectoryReaderTest
{
    static Logger log = LoggerFactory.getLogger( ConcurrentDirectoryReaderTest.class );
    
    ConcurrentDirectoryReader reader;

    Long totalFiles = 0L;
    Long eventsRead = 0L;
    
    @Before
    public void setUp() throws Exception
    {
        URL url = ConcurrentDirectoryReader.class.getResource( "/data-dir-3" );
        log.info( "Creating test-reader from directory '{}'", url );
        reader = new ConcurrentDirectoryReader( new File( url.toURI() ) );
        totalFiles = reader.bytesAvailable();
        log.info( "Found {} files.", totalFiles );
        eventsRead = 0L;
    }

    @Test
    public void testReadNext()
    {
        try {
            
            AuditEvent evt = reader.readNext();
            while( evt != null ){
                eventsRead++;

                log.info( "Completed {}%", 100 * ( eventsRead.doubleValue() / totalFiles.doubleValue() ) );
                try {
                    evt = reader.readNext();
                } catch (Exception e) {
                }
            }
            
            Assert.assertEquals( 3, eventsRead.intValue() );
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}