/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.io;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuditEventParserTest
{
    static Logger log = LoggerFactory.getLogger( AuditEventParserTest.class );

    @Before
    public void setUp() throws Exception {
    }

    
    /**
     * This is a test case for the GMT-date bug, making ModSecurity producing wrong dates
     * in timezones with negative GMT offset.
     */
    @Test
    public void testWebAuditBug5()
    {
        String dateString = "[14/Feb/2011:14:16:05 --0500] LchpWlV30w4AAD-aA1IAAAAA 10.0.0.102 60320 10.0.0.4 80";

        log.info( "################# WEB-AUDIT-5 BUG ##################" );
        log.info( "#" );
        Date date = AuditEventParser.parseDate( dateString );
        log.info( "Date: {}", date );
        SimpleDateFormat fmt = new SimpleDateFormat( "yyyyMMdd-HHmmss" );
        //
        // date string in event is:    14/Feb/2011:14:16:05 --0500
        //
        // if you run this test in germany, the hour will show "20" i.e. 8pm, which is due to
        // the GMT-5 of the event and germany being GMT+1  =>   14 + 6 hours =>  20
        //
        log.info( "   date: {}", fmt.format(date) );
        log.info( "   expecting: {}", "20110214-201605" );
        Assert.assertEquals( "20110214-201605", fmt.format( date ) );
        log.info( "#" );
        log.info( "################# WEB-AUDIT-5 BUG ##################" );
    }
}