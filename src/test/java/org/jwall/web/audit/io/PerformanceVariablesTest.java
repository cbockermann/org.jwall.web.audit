/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.io;

import java.net.URL;
import java.util.zip.GZIPInputStream;

import org.junit.Assert;
import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PerformanceVariablesTest {

	static Logger log = LoggerFactory.getLogger(PerformanceVariablesTest.class);

	@Test
	public void test() throws Exception {

		URL url = PerformanceVariablesTest.class
				.getResource("/sink-audit.log.gz");

		ModSecurity2AuditReader reader = new ModSecurity2AuditReader(
				new GZIPInputStream(url.openStream()));

		AuditEvent e = reader.readNext();
		while (e != null) {

			log.info("------------------------------------");
			log.info("AuditEvent  {}", e.get(ModSecurity.TX_ID));

			String p1 = e.get(ModSecurity.PERF_PHASE1);
			String p2 = e.get(ModSecurity.PERF_PHASE2);
			String p3 = e.get(ModSecurity.PERF_PHASE3);
			String p4 = e.get(ModSecurity.PERF_PHASE4);
			String p5 = e.get(ModSecurity.PERF_PHASE5);

			log.info("Phase1: {}ms", p1);
			log.info("Phase2: {}ms", p2);
			log.info("Phase3: {}ms", p3);
			log.info("Phase4: {}ms", p4);
			log.info("Phase5: {}ms", p5);

			for (String var : AuditEventParser.PERF_VARIABLES) {

				try {
					log.info("Value of '{}' is {}", var, new Long(e.get(var)));
					Assert.assertTrue("performance variable '" + var
							+ "' missing!", p1 != null && !p1.trim().isEmpty());
				} catch (NumberFormatException nfe) {
					log.error("Failed to parse variable {} with value {}", var,
							e.get(var));
				}
			}

			log.info("------------------------------------");
			e = reader.readNext();
		}

		reader.close();

		// fail("Not yet implemented");
	}
}
