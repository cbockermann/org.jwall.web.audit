/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.io;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GuessFormatTest {

	static Logger log = LoggerFactory.getLogger(GuessFormatTest.class);

	@Test
	public void test() {
		//
		// This test is disabled as the required data cannot be checked
		// into the repository due to privacy issues
		//

		// try {
		// File file = new File("/Volumes/RamDisk/craig/index");
		// int fmt = AuditFormat.guessFormat(file);
		// log.info("Format is: {}", fmt);
		// } catch (Exception e) {
		// // fail("Not yet implemented");
		// }
	}

}
