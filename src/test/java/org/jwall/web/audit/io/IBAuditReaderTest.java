/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.io;

import static org.junit.Assert.fail;

import java.net.URL;

import org.junit.Test;
import org.jwall.web.audit.AuditEventMessage;
import org.jwall.web.audit.IronBeeAuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IBAuditReaderTest {

	static Logger log = LoggerFactory.getLogger(IBAuditReaderTest.class);

	@Test
	public void testReadNext() {

		try {
			// URL url =
			// IBAuditReaderTest.class.getResource("/ironbee-audit.log");
			URL url = IBAuditReaderTest.class
					.getResource("/IB-95ebd277-b4c4-4951-97da-f9530205da39.log");
			log.info("Reading test-event from {}", url);
			IronBeeAuditReader reader = new IronBeeAuditReader(url.openStream());

			IronBeeAuditEvent event = (IronBeeAuditEvent) reader.readNext();
			log.info("Event = {}", event);

			log.info(" TX_ID = {}", event.get(ModSecurity.TX_ID));
			log.info("REQUEST_URI = {}", event.get(ModSecurity.REQUEST_URI));
			log.info("RESPONSE_STATUS = {}",
					event.get(ModSecurity.RESPONSE_STATUS));

			log.info("REQUEST_HEADERS:Host = {}",
					event.get(ModSecurity.REQUEST_HEADERS + ":HOST"));

			log.info("--------------------------------------------------");
			AuditEventMessage[] msgs = event.getEventMessages();
			for (AuditEventMessage m : msgs) {
				log.info("Message:   {}", m);
			}

			// do {
			// event = (IronBeeAuditEvent) reader.readNext();
			// log.info("Read event {}", event.getEventId());
			// } while (event != null);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Error: " + e.getMessage());
		}
	}
}
