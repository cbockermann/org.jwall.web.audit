/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.io;

import java.io.StringReader;
import java.util.Date;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.jwall.log.LogMessage;
import org.jwall.log.io.AccessLogReader;
import org.jwall.web.audit.ModSecurity;

public class AccessLogReaderTest {

	String line = "66.249.65.43 - - [22/Nov/2009:12:41:43 +0100] \"GET /web/policy/editor.jsp HTTP/1.1\" 200 6065 \"-\" \"Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)\"";
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testParse() throws Exception {
		AccessLogReader r = new AccessLogReader( new StringReader( line ) );
		LogMessage msg = r.readNext();
		Assert.assertEquals( "66.249.65.43", msg.get( ModSecurity.REMOTE_ADDR ) );
		Assert.assertEquals( "/web/policy/editor.jsp", msg.get( ModSecurity.REQUEST_URI ) );
		System.out.println( "Date: " + new Date(msg.getTimestamp()));
	}
}