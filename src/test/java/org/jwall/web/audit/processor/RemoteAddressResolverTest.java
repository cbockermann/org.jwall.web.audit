/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.processor;

import static org.junit.Assert.fail;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Test;
import org.jwall.event.test.EventList;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RemoteAddressResolverTest {

	static Logger log = LoggerFactory
			.getLogger(RemoteAddressResolverTest.class);

	@Test
	public void test() {

		List<AuditEvent> events = EventList.getAuditEvents(
				"/audit-event-orig-ip.log", 10);

		if (events.isEmpty())
			fail("Failed to load events for testing!");

		Map<String, Object> ctx = new LinkedHashMap<String, Object>();

		RemoteAddressResolver resolver = new RemoteAddressResolver();
		try {
			AuditEvent evt = events.get(0);

			String before = evt.get(ModSecurity.REMOTE_ADDR);
			log.info("Remote address BEFORE processing is: {}", before);
			Assert.assertEquals("172.16.0.1", before);

			log.info("Applying event-processor...");
			evt = resolver.processEvent(events.get(0), ctx);

			String remoteAddress = evt.get(ModSecurity.REMOTE_ADDR);
			log.info("Remote address after processing is: {}", remoteAddress);

			Assert.assertEquals("1.2.3.4", remoteAddress);

		} catch (Exception e) {
			fail("Test failed: " + e.getMessage());
		}
	}
}
