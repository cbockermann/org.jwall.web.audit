/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.processor;

import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.jwall.event.test.EventList;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventProcessorPipeline;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.io.IronBeeAuditReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RuleTagProcessorTest {

	static Logger log = LoggerFactory.getLogger(RuleTagProcessorTest.class);
	AuditEventProcessorPipeline pipeline;
	List<AuditEvent> events;

	@Before
	public void setUp() throws Exception {
		events = EventList
				.getAuditEvents("/default-request-host-audit.log", 10);
		pipeline = new AuditEventProcessorPipeline();
		// EventProcessorFinder pf = new EventProcessorFinder();
		// pf.deployCustomEventProcessors(
		// RuleTagProcessorTest.class.getResourceAsStream(
		// "/default-request-host.xml"), pipeline);
		RuleTagProcessor ruleTagger = new RuleTagProcessor();
		pipeline.register(1.0d, ruleTagger);
	}

	@Test
	public void testModSecurity() throws Exception {

		for (AuditEvent evt : events) {

			log.info("Testing tagging of event {}", evt.get(ModSecurity.TX_ID));
			String tagString = evt.get(AuditEvent.TAGS);
			log.info("    tags = {}", tagString);

			pipeline.process(evt);

			log.info("Tags: {}", evt.getAll(AuditEvent.TAGS));
			String after = evt.get(AuditEvent.TAGS);
			log.info("    tags processed event: {}", after);

			Assert.assertFalse("".equals(after.trim()));
		}
	}

	@Test
	public void testIronBee() throws Exception {
		IronBeeAuditReader reader = new IronBeeAuditReader(
				RuleTagProcessorTest.class
						.getResourceAsStream("/IB-95ebd277-b4c4-4951-97da-f9530205da39.log"));

		AuditEvent evt = reader.readNext();

		log.info("Tagging event {}", evt.getEventId());
		pipeline.process(evt);

		String tagString = evt.get(AuditEvent.TAGS);
		log.info("Tags processed: {}", tagString);
	}
}
