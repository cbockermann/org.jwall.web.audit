/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.processor;

import static org.junit.Assert.fail;

import java.net.URL;

import org.junit.Test;
import org.jwall.audit.EventProcessor;
import org.jwall.audit.processor.AuditEventProcessorFinder;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventProcessorPipeline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EventProcessorFinderTest {

	static Logger log = LoggerFactory.getLogger(EventProcessorFinderTest.class);

	@Test
	public void test() {

		AuditEventProcessorFinder finder = new AuditEventProcessorFinder();
		AuditEventProcessorPipeline pipeline = new AuditEventProcessorPipeline();

		URL url = EventProcessorFinderTest.class
				.getResource("/event-processors.xml");
		log.info("Adding event-processors from {}", url);

		try {
			finder.deployCustomEventProcessors(url.openStream(), pipeline);

			for (EventProcessor<AuditEvent> p : pipeline.getProcessors()) {
				log.info("Priority: {}, processor: {}",
						pipeline.getPriority(p), p);
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail("Not yet implemented");
		}
	}

}
