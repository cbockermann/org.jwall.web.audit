/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.processor;

import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.jwall.audit.EventProcessorFinder;
import org.jwall.audit.processor.DefaultRequestHost;
import org.jwall.event.test.EventList;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventProcessorPipeline;
import org.jwall.web.audit.ModSecurity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultRequestHostProcessorTest {

	static Logger log = LoggerFactory
			.getLogger(DefaultRequestHostProcessorTest.class);
	AuditEventProcessorPipeline pipeline;
	List<AuditEvent> events;

	@Before
	public void setUp() throws Exception {
		events = EventList
				.getAuditEvents("/default-request-host-audit.log", 10);
		pipeline = new AuditEventProcessorPipeline();
		EventProcessorFinder pf = new EventProcessorFinder();
		pf.deployCustomEventProcessors(DefaultRequestHostProcessorTest.class
				.getResourceAsStream("/default-request-host.xml"), pipeline);
	}

	@Test
	public void test() throws Exception {

		for (AuditEvent evt : events) {

			log.info("Testing default-request-host with event {}",
					evt.get(ModSecurity.TX_ID));
			String host = evt.get(ModSecurity.REQUEST_HEADERS + ":Host");
			log.info("    host = {}", host);

			pipeline.process(evt);

			String after = evt.get(ModSecurity.REQUEST_HEADERS + ":Host");
			log.info("    host of processed event: {}", after);

			if (host == null || "".equals(host.trim())) {
				Assert.assertEquals("", DefaultRequestHost.NOT_SET, after);
			} else {
				Assert.assertEquals("", host, after);
			}
		}
	}
}