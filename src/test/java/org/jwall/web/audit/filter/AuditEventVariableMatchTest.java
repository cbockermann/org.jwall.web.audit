/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.filter;

import java.net.URL;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.AuditFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuditEventVariableMatchTest
{
    static Logger log = LoggerFactory.getLogger( AuditEventVariableMatchTest.class );

    AuditEvent event;


    @Before
    public void setUp() throws Exception
    {
        URL url = AuditEventVariableMatchTest.class.getResource( "/test-audit.log" );
        log.info( "Test-events: " + url );
        AuditEventReader reader = AuditFormat.createReader( url.getFile(), false );
        event = reader.readNext();
    }


    @Test
    public void testMatches()
    {
        AuditEventFilter filter = new AuditEventFilter();

        AuditEventMatch match = new AuditEventMatch( "REQUEST_HEADERS:Host", Operator.EQ, "www.jwall.org" );
        filter.add( match );

        boolean matches = filter.matches( event );
        log.info( "Filter matches: " + matches );
        Assert.assertTrue( matches );

        filter.getMatches().clear();
        match = new AuditEventMatch( "SEVERITY", Operator.RX, "\\d+" );
        filter.add( match );
        matches = filter.matches( event );
        Assert.assertTrue( "", matches );
    }


    @Test
    public void testRuleDataMatch(){

        AuditEventFilter filter = new AuditEventFilter();
        AuditEventMatch match = new AuditEventMatch( ModSecurity.RULE_DATA, Operator.Contains, "please get in touch with me" );
        filter.add( match );
        
        try {
            
            URL url = AuditEventVariableMatchTest.class.getResource( "/rule-data-audit.log" );
            log.info( "Test-events: " + url );
            AuditEventReader reader = AuditFormat.createReader( url.getFile(), false );
            event = reader.readNext();
            
            boolean matches = filter.matches( event );
            Assert.assertTrue( matches );
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}