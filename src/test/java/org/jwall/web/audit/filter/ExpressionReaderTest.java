/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.filter;

import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ExpressionReaderTest {

	String exp1 = "   REQUEST_URI @eq \"jwall.org\"";
	String exp2 = "REQUEST_URI     @eq test";
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSkipWhiteSpace() {
		
		ExpressionReader r = new ExpressionReader( exp1 );
		r.skipWhiteSpace();
		
		Assert.assertTrue( r.getInputString().substring( r.getPosition() ).startsWith( "REQUEST_URI" ) );
	}

	@Test
	public void testReadVariable() {
		ExpressionReader r = new ExpressionReader( exp1 );
		String var;
		try {
			var = r.readVariable();
			Assert.assertEquals( "Unexpected variable!", "REQUEST_URI", var );
		} catch (FilterException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testReadOperator() {
		ExpressionReader r = new ExpressionReader( exp1 );
		try {
			r.readVariable();
			Operator op = r.readOperator();
			Assert.assertTrue( "Unexpected operator: ", op == Operator.EQ );
		} catch (Exception e) {
			fail( "Error: " + e.getMessage() );
		}
	}
	

	@Test
	public void testReadOperator2() {
		ExpressionReader r = new ExpressionReader( exp2 );
		try {
			r.readVariable();
			Operator op = r.readOperator();
			Assert.assertTrue( "Unexpected operator: ", op == Operator.EQ );
		} catch (Exception e) {
			fail( "Error: " + e.getMessage() );
		}
	}
	

	@Test
	public void testReadValue() {
		ExpressionReader r = new ExpressionReader( exp1 );
		
		try {
			r.readVariable();
			r.readOperator();
			String val = r.readValue();
			Assert.assertTrue( "Unexpected value: " + val, "jwall.org".equals( val ) );
		} catch (Exception e) {
			fail( "Error: " + e.getMessage() );
		}
	}
	
	@Test
	public void testReadSimpleExpression(){
		ExpressionReader r = new ExpressionReader( exp1 );
		try {
			AuditEventMatch m = (AuditEventMatch) r.readFilterExpression();
			Assert.assertEquals( "", m.getVariable(), "REQUEST_URI" );
			Assert.assertEquals( "", Operator.EQ, Operator.read( m.getOperator() ) );
			Assert.assertEquals( "", m.getValue(), "jwall.org" );
		} catch (Exception e) {
			fail( "Error: " + e.getMessage() );
		}
	}
	
	
	@Test
	public void testParseLimitted(){
		try {
			Set<String> vars = new HashSet<String>();
			vars.add( "REQUEST_URI" );
			ExpressionReader r = new ExpressionReader( "REQUEST_URI @eq test AND DOES_NOT_EXIST @eq 1" , vars );
			r.setStrictParsing( true );
			r.readFilterExpression();
			fail( "Error: Expected exception to be thrown!" );
		} catch (Exception e) {
			Assert.assertTrue( true );
		}
	}
	
	
	@Test
	public void testParseLimitted2(){
		try {
			Set<String> vars = new HashSet<String>();
			vars.add( "REQUEST_URI" );
			ExpressionReader r = new ExpressionReader( "REQUEST_URI @eq test AND REQUEST_HEADERS:Host @eq 1" , vars );
			r.setStrictParsing( true );
			r.readFilterExpression();
			fail( "Error: Expected exception to be thrown!" );
		} catch (Exception e) {
			Assert.assertTrue( true );
		}
	}
	
	@Test
	public void testParse(){
		String str = "RESPONSE_STATUS @eq 304  AND  REMOTE_ADDR @eq 193.170.134.158";
		try {
			FilterCompiler.parse( str );
		} catch (FilterException e) {
			fail( e.getMessage() );
		}
	}

	
	@Test
	public void testParse2(){
		String str = "RESPONSE_STATUS @eq 304  AND  ( REMOTE_ADDR @eq 193.170.134.158 )";
		try {
			FilterCompiler.parse( str );
		} catch (FilterException e) {
			fail( e.getMessage() );
		}
	}
}