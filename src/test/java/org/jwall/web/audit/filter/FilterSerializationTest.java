/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.filter;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FilterSerializationTest {
	
	static Logger log = LoggerFactory.getLogger( FilterSerializationTest.class );
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSerialization() throws Exception {

		FilterExpression f = FilterCompiler.parse( "REMOTE_ADDR @eq 127.0.0.1" );
		String str = f.toString();
		log.info( "Filter before serialization is: {}", str );
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream( baos );
		oos.writeObject( f );
		oos.close();
		baos.close();
		
		ByteArrayInputStream bais = new ByteArrayInputStream( baos.toByteArray() );
		ObjectInputStream ois = new ObjectInputStream( bais );
		FilterExpression ex = (FilterExpression) ois.readObject();
		ois.close();
		
		String str2 = ex.toString();
		log.info( "De-serialized filter is: {}" + str2 );
		Assert.assertEquals( str, str2 );
	}
}
