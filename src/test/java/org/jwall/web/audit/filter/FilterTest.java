/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.filter;


import java.net.URL;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.AuditFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FilterTest {
	static Logger log = LoggerFactory.getLogger( FilterTest.class );
	AuditEvent event;
	
	@Before
	public void setUp() throws Exception {
        URL url = AuditEventVariableMatchTest.class.getResource( "/test-audit.log" );
        log.info( "Test-events: " + url );
        AuditEventReader reader = AuditFormat.createReader( url.getFile(), false );
        event = reader.readNext();
	}
	
	@Test
	public void testExp1() throws FilterException {
		boolean b = ExpressionReader.parse( "REQUEST_HEADERS:Host @eq www.jwall.org" ).matches( event );
		Assert.assertTrue( b );
	}
	
	@Test
	public void testExp2() throws FilterException {
		boolean b = ExpressionReader.parse( "REQUEST_HEADERS:Host @eq secure.jwall.org" ).matches( event );
		Assert.assertFalse( b );
	}
	
	@Test
	public void testExp3() throws FilterException {
		boolean b = ExpressionReader.parse( "REQUEST_HEADERS:Host @sx *jwall.org" ).matches( event );
		Assert.assertTrue( b );
	}
	
	@Test
	public void testExp4() throws FilterException {
		boolean b = ExpressionReader.parse( "REQUEST_HEADERS:Host @sx *jwall.org AND REQUEST_URI !@eq '/robots.txt'" ).matches( event );
		Assert.assertFalse( b );
	}
	
	@Test
	public void testExp5() throws FilterException {
		boolean b = ExpressionReader.parse( "REQUEST_URI @eq !'/robots.txt' and REQUEST_HEADERS:Host @sx *jwall.org AND REQUEST_METHOD @rx '!(GET|POST)'" ).matches( event );
		Assert.assertFalse( b );
	}
}