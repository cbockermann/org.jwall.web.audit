/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.rules;

import static org.junit.Assert.fail;

import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.jwall.event.test.EventList;
import org.jwall.web.audit.AuditEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScoreExtractorTest {

	static Logger log = LoggerFactory.getLogger( ScoreExtractorTest.class );

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testExtract() {

		List<AuditEvent> events = EventList.getAuditEvents( "/default-request-host-audit.log", 10 );

		try {

			PersistentCollectionExtractor ex = new PersistentCollectionExtractor();
			String variable = "TX:ANOMALY_SCORE";

			for( AuditEvent evt : events ){
				log.info( "Extracting {} from event {}", variable, evt.getEventId() );

				//log.info( "Event is:\n{}", evt );

				Map<String,String> env = PersistentCollectionExtractor.extractScores( evt );
				for( String key : env.keySet() ){
					log.debug( "   {} = {}", key, env.get(key) );
				}
				
				String value = ex.extract( "TX:ANOMALY_SCORE", evt );
				log.info( "Extracted value is {}", value );
				
				Assert.assertEquals( "5", ex.extract( "TX:ANOMALY_SCORE", evt ) );
				Assert.assertEquals( "5", ex.extract( "tx.anomaly_score", evt ) );
				Assert.assertEquals( "5", ex.extract( "tx.inbound_anomaly_score", evt ) );
				//waitForReturn();
			}			
		} catch (Exception e) {
			fail("Not yet implemented");
		}
	}
	
	public static void waitForReturn(){
		try {
			System.out.print( "Press return to continue..." );
			System.in.read();
		} catch (Exception e) {
		}
	}
}
