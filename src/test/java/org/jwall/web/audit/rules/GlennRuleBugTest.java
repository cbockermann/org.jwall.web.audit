package org.jwall.web.audit.rules;

import static org.junit.Assert.fail;

import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.jwall.event.test.EventList;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.rules.operators.ConditionEQ;
import org.jwall.web.audit.rules.operators.ConditionSX;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GlennRuleBugTest {

	static Logger log = LoggerFactory.getLogger(GlennRuleBugTest.class);

	@Test
	public void test() {
		try {
			String loc = "/bugs/glenn-rule-bug-audit.log";
			List<AuditEvent> evts = EventList.getAuditEvents(loc, 2);
			log.info("Read {} events from {}", evts.size(), loc);
			Assert.assertTrue(evts.size() == 1);

			AuditEventRule rule = new AuditEventRule();
			rule.add(new ConditionEQ("REQUEST_METHOD", "POST"));
			rule.add(new ConditionEQ("REQUEST_URI", "/auth/password"));
			rule.add(new ConditionSX("REMOTE_ADDR", "!192.168.*"));
			rule.setMatchAll(true);

			boolean match = rule.matches(evts.get(0), null);
			log.info("Rule matches? {}", match);
			Assert.assertTrue(match);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Test failed: " + e.getMessage());
		}
	}
}
