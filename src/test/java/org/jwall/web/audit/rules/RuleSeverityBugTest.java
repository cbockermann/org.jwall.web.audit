/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.rules;


import java.net.URL;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.ModSecurity2AuditReader;
import org.jwall.web.audit.rules.operators.ConditionLT;
import org.jwall.web.audit.test.ParserBugConsole8Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RuleSeverityBugTest
{
    static Logger log = LoggerFactory.getLogger( ParserBugConsole8Test.class );
    AuditEventReader reader;

    @Before
    public void setUp() throws Exception
    {
        URL url = ParserBugConsole8Test.class.getResource( "/rule-sev-extraction-bug.dat" );
        log.info( "Audit-ScriptEvent-Log: {}", url );
        reader = new ModSecurity2AuditReader( url.openStream() );
    }

    @Test
    public void test() {
 
        try {
            AuditEvent event = reader.readNext();

            ConditionLT lt = new ConditionLT( ModSecurity.RULE_SEV, "3" );
            
            List<String> values = ValueExtractor.extractValues( ModSecurity.RULE_SEV, event );
            boolean matches = lt.matches( values );
            Assert.assertFalse( matches );
            
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail( e.getMessage() );
        }
    }
}