/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.rules;

import static org.junit.Assert.fail;

import java.io.File;
import java.net.URL;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.AuditFormat;
import org.jwall.web.audit.rules.operators.ConditionEQ;
import org.jwall.web.audit.rules.operators.ConditionRX;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.XStream;

public class EventRuleConditionTests {
	AuditEvent event;
	static Logger log = LoggerFactory.getLogger(EventRuleConditionTests.class);

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		File resourceDir = new File("src/test/resources");
		File auditLog = new File(resourceDir.getAbsolutePath() + File.separator
				+ "test-audit.log");
		log.info("Reading audit-event from file '" + auditLog.getAbsolutePath()
				+ "'");
		AuditEventReader reader = AuditFormat.createReader(
				auditLog.getAbsolutePath(), false);
		event = reader.readNext();
	}

	@Test
	public void testMatches() {
		try {
			AuditEventRule rule = new AuditEventRule();
			rule.add(new ConditionRX("SEVERITY", "^4$"));

			boolean matches = rule.matches(event, null);
			log.info(" rule matches??: " + matches);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Not yet implemented");
		}
	}

	@Test
	public void testCountOperator() {
		try {
			AuditEventRule rule = new AuditEventRule();

			Condition condition = new ConditionEQ("&REQUEST_HEADERS:Host", "1");
			rule.add(condition);

			boolean matches = rule.matches(event, null);

			if (!matches)
				fail("Count-operator failed!");

		} catch (Exception e) {
			e.printStackTrace();
			fail("Error: '" + e.getMessage() + "'");
		}

	}

	@Test
	public void testSeverityEq() {

		String severity = event.get(ModSecurity.RULE_SEV);
		severity = ValueExtractor.extractValues(ModSecurity.RULE_SEV, event)
				.get(0);
		log.info("Severity: {}", severity);
	}

	@Test
	public void testRuleSeverityEq2() throws Exception {

		URL url = EventRuleConditionTests.class.getResource("/event-rule.xml");

		XStream xs = new XStream();
		for (Class<?> clazz : AuditEventRule.CLASSES) {
			xs.processAnnotations(clazz);
		}

		AuditEventRule rule = (AuditEventRule) xs.fromXML(url);

		boolean match = rule.matches(event, null);
		Assert.assertTrue(match);
	}

	@Test
	public void testRuleSeverityEq3() throws Exception {

		URL url = EventRuleConditionTests.class
				.getResource("/event-rule-severity-eq-3.xml");

		XStream xs = new XStream();
		for (Class<?> clazz : AuditEventRule.CLASSES) {
			xs.processAnnotations(clazz);
		}

		AuditEventRule rule = (AuditEventRule) xs.fromXML(url);
		boolean match = rule.matches(event, null);
		Assert.assertFalse(match);
	}

}