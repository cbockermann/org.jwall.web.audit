/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.rules;

import static org.junit.Assert.fail;

import java.net.URL;
import java.util.Map;

import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.ModSecurity2AuditReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScoreBugTest {

	static Logger log = LoggerFactory.getLogger(ScoreBugTest.class);

	@Test
	public void test() {

		try {
			URL url = ScoreBugTest.class.getResource("/score-bug-audit.log");
			AuditEventReader reader = new ModSecurity2AuditReader(
					url.openStream());

			AuditEvent evt = reader.readNext();

			PersistentCollectionExtractor coll = new PersistentCollectionExtractor();
			Map<String, String> scores = coll.extractScores(evt);
			for (String key : scores.keySet()) {
				log.info("Score for '{}' = {}", key, scores.get(key));
			}

		} catch (Exception e) {
			e.printStackTrace();
			fail("Test failed: " + e.getMessage());
		}
	}
}