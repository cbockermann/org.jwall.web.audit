/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.rules;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.jwall.web.audit.rules.operators.ConditionEQ;

public class ConditionEQTest {

	List<String> positive = new ArrayList<String>();
	List<String> negative = new ArrayList<String>();

	@Before
	public void setUp() throws Exception {
		positive.add("A");
		positive.add("B");
	}

	@Test
	public void testMatchesListOfString() throws Exception {
		ConditionEQ eq = new ConditionEQ("", "B");
		boolean rc = eq.matches(positive);
		Assert.assertTrue(rc);
	}

	@Test
	public void testNegated() throws Exception {
		ConditionEQ eq = new ConditionEQ("", "!B");
		boolean rc = eq.matches(positive);
		// Assert.assertFalse( rc );
	}
}