/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.rules;


import java.io.File;
import java.util.List;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.AuditFormat;
import org.jwall.web.audit.util.TimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AgeVariableTest {
    static AuditEvent event;
    static Logger log = LoggerFactory.getLogger( AgeVariableTest.class );

    @BeforeClass
    public static void setUpBeforeClass() throws Exception
    {
    	File resourceDir = new File( "src/test/resources" );
    	File auditLog = new File( resourceDir.getAbsolutePath() + File.separator + "test-audit.log" );
    	log.info( "Reading audit-event from file '" + auditLog.getAbsolutePath() + "'" );
    	AuditEventReader reader = AuditFormat.createReader( auditLog.getAbsolutePath(), false );
    	event = reader.readNext();
    }
    
    @Test
    public void testAge(){
    	List<String> list = ValueExtractor.extractValues( AuditEvent.AGE, event );
    	Assert.assertEquals( 1, list.size() );
    	String age = list.get( 0 );
    	
    	Long ageInMillis = new Long( age );
    	Long expAge = System.currentTimeMillis() - event.getDate().getTime();
    	log.info( "ScriptEvent date is: {}", event.getDate() );
    	log.info( "ScriptEvent age is: {}  =>  {}", age, TimeFormat.formatLong( ageInMillis ) );
    	log.info( "Expected age: {}  =>  {}", expAge, TimeFormat.formatLong( expAge ) );
    	Assert.assertTrue( ageInMillis.equals( expAge ) );
    }
}