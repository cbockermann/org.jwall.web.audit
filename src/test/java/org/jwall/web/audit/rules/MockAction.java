/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.rules;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.jwall.web.audit.AuditEvent;

public class MockAction implements EventAction {

	@Override
	public void execute(Map<String, Object> ctx, AuditEvent evt) {
		System.out.println( "Firing action on event: " + evt );
	}

	@Override
	public String getLabel() {
		return "FAKE_ACTION";
	}

	@Override
	public String getName() {
		return "FAKE_ACTION";
	}

	@Override
	public Map<String, String> getParameters() {
		return new LinkedHashMap<String,String>();
	}

	@Override
	public List<String> getValues() {
		return new ArrayList<String>();
	}
}