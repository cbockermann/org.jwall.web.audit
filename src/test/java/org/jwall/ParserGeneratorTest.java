/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall;

import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.jwall.log.io.Parser;
import org.jwall.log.io.ParserGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ParserGeneratorTest
{
    static Logger log = LoggerFactory.getLogger( ParserGeneratorTest.class );
    
    @Before
    public void setUp() throws Exception
    {
    }

    @Test
    public void testCreate() throws Exception
    {
        String grammar = "%{REMOTE_ADDR}\" %{REMOTE_PORT} TEST [ABC] %{ABC}";
        String input = "127.0.0.1\" 80 TEST [ABC] abc-value";
        ParserGenerator gen = new ParserGenerator( grammar );

        Parser<Map<String,String>> parser = gen.newParser();
        Map<String,String> msg = parser.parse( input );
        log.info( "Message parsed is: {}", msg );
        for( String key : msg.keySet() ){
            log.info( "  {} = {}", key, msg.get( key ) );
        }
        
        Assert.assertTrue( "127.0.0.1".equals( msg.get( "REMOTE_ADDR" ) ) );
    }
    
    @Test
    public void testAccessLogCombined() throws Exception {
        String input = "www-ai.cs.uni-dortmund.de www-ai.cs.uni-dortmund.de 66.249.72.167 - - [08/Jul/2011:00:01:29 +0200] \"GET /FUNSTUFF/JAI/index.html HTTP/1.1\" 200 3487 \"-\" \"Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)\"";
        String grammar = "%{REQUEST_HEADERS:Host} %{VHOST_NAME} %{REMOTE_ADDR} %{REMOTE_USER} %{TMP} [%{DATE}] \"%{REQUEST_METHOD} %{REQUEST_URI} %{PROTO}\" %{RESPONSE_STATUS} %{RESPONSE_SIZE} \"%{REQUEST_HEADERS:Referer}\" \"%{REQUEST_HEADERS:User-Agent}\"";
        
        ParserGenerator gen = new ParserGenerator( grammar );
        Map<String,String> map = gen.parse( input );
        for( String key : map.keySet() ){
            log.info( " '{}' = '{}'", key, map.get( key ) );
        }
        
        List<String> grams = gen.parseGrammar( grammar );
        for( String g : grams ){
            log.info( "{}", g );
        }
    }
    
    @Test
    public void testParseGrammar() throws Exception {
        String grammar = "%{REQUEST_HEADERS:Host} %{VHOST_NAME} %{REMOTE_ADDR} %{REMOTE_USER} %{TMP} [%{DATE}] \"%{REQUEST_METHOD} %{REQUEST_URI} %{PROTO}\" %{RESPONSE_STATUS} %{RESPONSE_SIZE} \"%{REQUEST_HEADERS:Referer}\" \"%{REQUEST_HEADERS:User-Agent}\"";
        ParserGenerator gen = new ParserGenerator( grammar );
        List<String> grams = gen.parseGrammar( grammar );
        int i = 0;
        for( String g : grams ){
            log.info( "{}: '{}'", i++, g );
        }
    }
    
    
    public static void main( String args[] ) throws Exception {
    	
    	
    	String grammar = "[%{BEGIN} - %{END}]";
    	String input = "[-3.4 - 8.43]";
    	
    	ParserGenerator gen = new ParserGenerator( grammar );
    	
    	Parser<Map<String,String>> parser = gen.newParser();
    	Map<String,String> data = parser.parse( input );
    	
    	for( String key : data.keySet() ){
    		System.out.println( key + " = " + data.get( key ) );
    	}
    }
}