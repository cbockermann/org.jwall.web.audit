/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.event.test;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.io.ModSecurity2AuditReader;

public class EventList {
	
	
	
	

	public static List<AuditEvent> getAuditEvents( String resource, int num ){
		List<AuditEvent> list = new ArrayList<AuditEvent>();
		
		try {
			int i = 0;
			URL url = EventList.class.getResource( resource );
			ModSecurity2AuditReader reader = new ModSecurity2AuditReader( url.openStream() );
			AuditEvent event = reader.readNext();
			while( event != null && i < num ){
				list.add( event );
				event = reader.readNext();
			}

			
		} catch (Exception e) {
			
		}
		
		return list;
	}
	

	public static List<AuditEvent> getAuditEvents( int num ){
		List<AuditEvent> list = new ArrayList<AuditEvent>();
		
		try {
			int i = 0;
			URL url = EventList.class.getResource( "/events.audit-log" );
			ModSecurity2AuditReader reader = new ModSecurity2AuditReader( url.openStream() );
			AuditEvent event = reader.readNext();
			while( event != null && i < num ){
				list.add( event );
				event = reader.readNext();
			}

			
		} catch (Exception e) {
			
		}
		
		return list;
	}
}
