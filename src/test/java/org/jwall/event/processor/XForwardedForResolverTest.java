/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.event.processor;

import java.net.URL;
import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.io.ModSecurity2AuditReader;
import org.jwall.web.audit.processor.XForwardedForResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XForwardedForResolverTest {

	static Logger log = LoggerFactory.getLogger( XForwardedForResolverTest.class );
	XForwardedForResolver resolver = new XForwardedForResolver();
	AuditEvent event;

	@Test
	public void testProcessEvent() {

		log.info( "Running resolver-test..." );
		try {
			URL url = XForwardedForResolverTest.class.getResource( "/test-audit.log" );
			ModSecurity2AuditReader reader = new ModSecurity2AuditReader( url.openStream() );
			event = reader.readNext();
		} catch (Exception e) {
			Assert.fail( e.getMessage() );
		}
		
		Assert.assertNotNull( event );
		
		//String remoteAddr = event.get( ModSecurity.REMOTE_ADDR );
		String fwd = event.get( ModSecurity.REQUEST_HEADERS + ":X-Forwarded-For".toUpperCase() );
		
		try {
			AuditEvent processed = resolver.processEvent( event, new HashMap<String,Object>() );
			
			String remote = processed.get( ModSecurity.REMOTE_ADDR );
			Assert.assertEquals( remote, fwd );
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail( e.getMessage() );
		}
	}
}