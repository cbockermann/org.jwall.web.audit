/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.audit.script;

import java.util.ListIterator;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.jwall.audit.script.utils.ListIteratorImpl;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventDatabaseMock;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.filter.FilterCompiler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListIteratorTest {

	final static AuditEventDatabaseMock database = new AuditEventDatabaseMock();
	static Logger log = LoggerFactory.getLogger(ListIteratorTest.class);
	static EventList<AuditEvent> eventList;

	@BeforeClass
	public static void setup() throws Exception {
		try {
			eventList = new EventList<AuditEvent>(database,
					FilterCompiler.parse(""));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testHasNext() throws Exception {
		ListIterator<ScriptEvent> it = new ListIteratorImpl(eventList);
		boolean containsEvents = !database.getRealList().isEmpty();
		boolean hasNext = it.hasNext();
		Assert.assertEquals(containsEvents, hasNext);
	}

	@Test
	public void testHasPrevious() {
		ListIterator<ScriptEvent> it = new ListIteratorImpl(eventList);
		Assert.assertFalse(it.hasPrevious());
		it.next();
		Assert.assertTrue(it.hasPrevious());
	}

	@Test
	public void testNext() {
		ListIterator<ScriptEvent> it = new ListIteratorImpl(eventList);

		ScriptEvent evt = it.next();
		Assert.assertNotNull(evt);

		AuditEvent first = database.getRealList().get(0);
		Assert.assertEquals(first.get(ModSecurity.TX_ID),
				evt.get(ModSecurity.TX_ID));
	}

	@Test
	public void testNextIndex() {
		ListIterator<ScriptEvent> it = new ListIteratorImpl(eventList);

		ScriptEvent evt = it.next();
		Assert.assertNotNull(evt);

		AuditEvent first = database.getRealList().get(0);
		Assert.assertEquals(first.get(ModSecurity.TX_ID),
				evt.get(ModSecurity.TX_ID));

		Assert.assertEquals(1, it.nextIndex());
	}

	@Test
	public void testPrevious() {
		ListIterator<ScriptEvent> it = new ListIteratorImpl(eventList);

		ScriptEvent evt = it.next();
		Assert.assertNotNull(evt);

		AuditEvent first = database.getRealList().get(0);

		it.next();
		it.previous();
		evt = it.previous();
		Assert.assertEquals(first.get(ModSecurity.TX_ID),
				evt.get(ModSecurity.TX_ID));
	}

	@Test
	public void testPreviousIndex() {
		ListIterator<ScriptEvent> it = new ListIteratorImpl(eventList);

		ScriptEvent evt = it.next();
		Assert.assertNotNull(evt);

		AuditEvent first = database.getRealList().get(0);
		Assert.assertEquals(first.get(ModSecurity.TX_ID),
				evt.get(ModSecurity.TX_ID));

		Assert.assertEquals(0, it.previousIndex());
	}
}