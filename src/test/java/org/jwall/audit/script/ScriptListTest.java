/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.audit.script;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;

import org.junit.Assert;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventDatabaseMock;
import org.jwall.web.audit.ModSecurity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScriptListTest {

	final static AuditEventDatabaseMock database = new AuditEventDatabaseMock();
	static Logger log = LoggerFactory.getLogger(ScriptListTest.class);

//	// @Test
//	public void testListAll() throws Exception {
//
//		URL rubyScript = ScriptListTest.class.getResource("/list-all.rb");
//
//		StringWriter exp = new StringWriter();
//		PrintWriter p = new PrintWriter(exp);
//
//		for (AuditEvent evt : database.getRealList()) {
//			p.write(evt.get(ModSecurity.TX_ID) + "\n");
//		}
//
//		for (AuditEvent evt : database.getRealList()) {
//			if ("500".equals(evt.get(ModSecurity.RESPONSE_STATUS)))
//				p.write(evt.get(ModSecurity.TX_ID) + "\n");
//		}
//
//		ScriptRunner exec = new ScriptRunner(rubyScript, "ruby");
//
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
//		PrintStream out = new PrintStream(baos);
//		exec.init(out, System.err);
//		exec.setAuditEventView(database);
//		exec.execute();
//		log.debug("Output is:\n{}", new String(baos.toByteArray()));
//		Assert.assertEquals(exp.toString(), new String(baos.toByteArray()));
//	}
}