/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.audit.script;

import java.io.ByteArrayOutputStream;
import java.net.URL;

import org.junit.Assert;
import org.junit.Test;
import org.jwall.audit.script.utils.TimePrintStream;
import org.jwall.web.audit.AuditEventDatabaseMock;
import org.jwall.web.audit.filter.FilterCompiler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScriptDeleteTest {

	final static AuditEventDatabaseMock database = new AuditEventDatabaseMock();
	static Logger log = LoggerFactory.getLogger( ScriptDeleteTest.class );
//
//
//	@Test
//	public void testExecCount500FromUrl() throws Exception {
//
//		try {
//			URL rubyScript = ScriptDeleteTest.class.getResource( "/delete-response500.rb" );
//
//			Long total = database.count( FilterCompiler.parse( "" ) );
//			Long error500 = database.count( FilterCompiler.parse( "RESPONSE_STATUS = 500" ) );
//
//			log.info( "database has {} events in total and {} events with status=500", total, error500 );
//
//			ScriptRunner exec = new ScriptRunner( rubyScript, "ruby" );
//
//			ByteArrayOutputStream baos = new ByteArrayOutputStream();
//			exec.init( new TimePrintStream( baos), System.err );
//			exec.setAuditEventView( database );
//			exec.execute();
//
//			log.debug( "Output:\n{}", new String( baos.toByteArray() ) );
//			
//			Long remain = database.count( FilterCompiler.parse( "" ) );
//			log.info( "after script-execution, the database contains a total of {} events", remain );
//			//Assert.assertEquals( total - error500, remain.intValue() );
//		} catch (Exception e) {
//			e.printStackTrace();
//			Assert.fail( "Error: " + e.getMessage() );
//		}
//	}
}