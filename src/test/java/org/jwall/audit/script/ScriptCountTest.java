/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.audit.script;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.net.URL;

import org.junit.Assert;
import org.junit.Test;
import org.jwall.web.audit.AuditEventDatabaseMock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScriptCountTest {
	
	final static AuditEventDatabaseMock database = new AuditEventDatabaseMock();
	static Logger log = LoggerFactory.getLogger( ScriptCountTest.class );
	
	
//	@Test
//	public void test() throws Exception {
//		
//		String ruby = "cnt = $events.count( \"\" )\n"
//				+ "puts \"Database contains #{cnt} events.\"\n";
//		
//		String exp = "Database contains " + database.getRealList().size() + " events.\n";
//		
//		ScriptRunner exec = new ScriptRunner( ruby, "ruby" );
//		
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
//		PrintStream out = new PrintStream( baos );
//		
//		exec.init( out, System.err );
//		exec.setAuditEventView( database );
//		exec.execute();
//
//		String output = new String( baos.toByteArray() );
//		Assert.assertEquals( exp, output );
//	}
//	
//	
//	
//	@Test
//	public void testExecFromUrl() throws Exception {
//		
//		URL rubyScript = ScriptCountTest.class.getResource( "/count.rb" );
//		
//		String exp = "Database contains " + database.getRealList().size() + " events.\n";
//		
//		ScriptRunner exec = new ScriptRunner( rubyScript, "ruby" );
//		
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
//		PrintStream out = new PrintStream( baos );
//		
//		exec.init( out, System.err );
//		exec.setAuditEventView( database );
//		exec.execute();
//
//		String output = new String( baos.toByteArray() );
//		log.debug("output: {}", output );
//		log.debug("expect: {}", exp );
//		Assert.assertEquals( exp, output );
//	}
//
//	
//	
//	
//	
//	@Test
//	public void testExecCount500FromUrl() throws Exception {
//		
//		URL rubyScript = ScriptCountTest.class.getResource( "/count-response500.rb" );
//		
//		String exp = "Database contains 6 events.\n";
//		
//		ScriptRunner exec = new ScriptRunner( rubyScript, "ruby" );
//		
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
//		PrintStream out = new PrintStream( baos );
//		
//		exec.init( out, System.err );
//		exec.setAuditEventView( database );
//		exec.execute();
//
//		String output = new String( baos.toByteArray() );
//		Assert.assertEquals( exp, output );
//	}
}