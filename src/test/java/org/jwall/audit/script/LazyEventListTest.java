/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.audit.script;

import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventDatabaseMock;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.filter.FilterCompiler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LazyEventListTest {

	final static AuditEventDatabaseMock database = new AuditEventDatabaseMock();
	static Logger log = LoggerFactory.getLogger(LazyEventListTest.class);

	@BeforeClass
	public static void setup() throws Exception {
		log.debug("Database mock is: {}", database);
	}

	@Test
	public void testSize() throws Exception {
		List<ScriptEvent> list = new EventList<AuditEvent>(database,
				FilterCompiler.parse(""));
		Assert.assertEquals(list.size(),
				database.count(FilterCompiler.parse("")).intValue());
	}

	@Test
	public void testIsEmpty() throws Exception {
		List<ScriptEvent> list = new EventList<AuditEvent>(
				database,
				FilterCompiler
						.parse("REQUEST_URI = /this-page-does-not-exist-in-the-test-data-set.html"));
		Assert.assertEquals(list.isEmpty(), true);
	}

	@Test
	public void testClear() throws Exception {
		List<ScriptEvent> list = new EventList<AuditEvent>(database,
				FilterCompiler.parse(""));
		Assert.assertEquals(list.size(),
				database.count(FilterCompiler.parse("")).intValue());
		log.info("list contains {} elements", list.size());
		list.clear();
		Assert.assertTrue(list.isEmpty());
	}

	@Test
	public void testGetInt() throws Exception {
		List<ScriptEvent> list = new EventList<AuditEvent>(database,
				FilterCompiler.parse(""));
		Assert.assertEquals(list.size(),
				database.count(FilterCompiler.parse("")).intValue());
		Assert.assertNull(list.get(-1));
	}

	@Test
	public void testGetInt2() throws Exception {
		List<ScriptEvent> list = new EventList<AuditEvent>(database,
				FilterCompiler.parse(""));
		Assert.assertEquals(list.size(),
				database.count(FilterCompiler.parse("")).intValue());

		ScriptEvent e1 = list.get(2);
		String id1 = e1.get(ModSecurity.TX_ID);

		AuditEvent e2 = database.getRealList().get(2);
		String id2 = e2.get(ModSecurity.TX_ID);

		Assert.assertEquals(id2, id1);
	}
}