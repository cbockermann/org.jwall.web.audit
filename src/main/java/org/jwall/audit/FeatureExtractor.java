/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.audit;

import java.util.Set;

public interface FeatureExtractor<E extends Event, F> {

	/**
	 * This method extracts all available feature names for the given event.
	 * 
	 * @param event
	 * @return
	 */
	public Set<String> getVariables( E event );
	
	
	/**
	 * Extract all values for the specified feature from the given event.
	 * 
	 * @param feature
	 * @param event
	 * @return
	 */
	public F extract( String feature, E event );
}