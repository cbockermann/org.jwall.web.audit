/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.audit.processor;

import java.util.List;
import java.util.Map;

import org.jwall.audit.EventProcessor;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;

/**
 * This simple event processor fills in a default host value for events that do
 * not come with a request header including a <code>Host:</code>-field.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class DefaultRequestHost implements EventProcessor<AuditEvent> {
	public final static String NOT_SET = "N/A";

	String value = NOT_SET;

	/**
	 * @see org.jwall.audit.EventProcessor#processEvent(org.jwall.audit.Event,
	 *      java.util.Map)
	 */
	@Override
	public AuditEvent processEvent(AuditEvent event, Map<String, Object> context)
			throws Exception {

		List<String> values = event.getAll(ModSecurity.REQUEST_HEADERS
				+ ":Host");
		if (!event.isSet(ModSecurity.REQUEST_HEADERS + ":Host")
				|| values == null || values.isEmpty()
				|| "".equals(values.get(0).trim())) {
			event.set(ModSecurity.REQUEST_HEADERS + ":Host".toUpperCase(),
					value);
		}

		return event;
	}

	/**
	 * A simple getter for the default host value.
	 * 
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 * A simple setter for the default host value.
	 * 
	 * @param defaultHost
	 */
	public void setValue(String defaultHost) {
		this.value = defaultHost;
	}
}