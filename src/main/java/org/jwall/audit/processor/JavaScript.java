/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.audit.processor;

import java.util.Map;

import org.jwall.audit.EventProcessor;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class JavaScript extends stream.script.JavaScript implements
		EventProcessor<AuditEvent> {
	static Logger log = LoggerFactory.getLogger(JavaScript.class);

	@Override
	public AuditEvent processEvent(AuditEvent event, Map<String, Object> context)
			throws Exception {

		if (event == null) {
			log.debug("Event is 'null' - skipping JavaScript processing.");
			return event;
		}

		this.scriptEngine.put("context", context);
		this.scriptEngine.put("event", event);
		String theScript = loadScript();

		log.debug("Evaluating JavaScript for event '{}'",
				event.get(ModSecurity.TX_ID));
		scriptEngine.eval(theScript);
		log.debug("Returning event...");
		return event;
	}
}