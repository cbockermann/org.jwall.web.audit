/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.audit.processor;

import java.util.Map;

import org.jwall.audit.Event;
import org.jwall.audit.EventProcessor;

public abstract class AbstractEventProcessor<E extends Event> 
	implements EventProcessor<E> 
{
	/** This processor's priority */
	Double priority = 100.0d;
	
	/** This flag signals whether this processor is enabled or not */
	boolean enabled = true;
	
	
	public AbstractEventProcessor( Double defaultPriority ){
		priority = defaultPriority;
	}
	

	@Override
	public E processEvent(E event, Map<String, Object> context) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	

	/**
	 * @return the priority
	 */
	public Double getPriority() {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(Double priority) {
		this.priority = priority;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isEnabled() {
		return enabled;
	}
}