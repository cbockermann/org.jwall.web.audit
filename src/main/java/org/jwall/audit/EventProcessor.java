/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.audit;

import java.util.Map;


/**
 * <p>
 * This interface defines an abstract event processor class which is able
 * to process events for a given generic type class.
 * </p>
 * <p>
 * In contrast to an EventListener, classes implementing this interface are
 * responsible for any pre-processing of an event.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 * @param <E>
 */
public interface EventProcessor<E extends Event> {

	public final static String DELETE_FLAG = "event.flag.delete";
	
	
	/**
	 * This method is called upon event reception. The given map provides a context to 
	 * communicate data between several event-processors. The implementor of this interface
	 * needs to take care that calls to this class my come from different threads, i.e.
	 * no state should be stored in the class to diminish the risk of concurrency problems.
	 * 
	 * @param event
	 * @param context
	 * @return
	 * @throws Exception
	 */
	public E processEvent( E event, Map<String,Object> context ) throws Exception;
}
