/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.audit.rules;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jwall.audit.Event;
import org.jwall.audit.EventProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * A simple engine that will sequentially process rules for an event. If any of
 * that rules does flag the event as DELETED, then rule processing will be
 * stopped.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 * @param <E>
 */
public abstract class EventRuleEngine<E extends Event> implements
		EventProcessor<E> {
	static Logger log = LoggerFactory.getLogger(EventRuleEngine.class);

	/** The list of rules processed by this engine */
	List<EventRule<E>> rules = new ArrayList<EventRule<E>>();

	/**
	 * @return the rules
	 */
	public List<EventRule<E>> getRules() {
		return rules;
	}

	/**
	 * @param rules
	 *            the rules to set
	 */
	public void setRules(List<EventRule<E>> rules) {
		this.rules = rules;
	}

	/**
	 * @see org.jwall.audit.EventProcessor#processEvent(org.jwall.audit.Event,
	 *      java.util.Map)
	 */
	@Override
	public final E processEvent(E event, Map<String, Object> context)
			throws Exception {
		final List<EventRule<E>> rules = getRules();
		log.debug("Processing {} rules for event {}", rules.size(), event);

		for (EventRule<E> rule : rules) {
			if (matches(rule, event, null)) {
				log.debug("Rule {} matches, firing actions", rule);
				execute(rule, event, context);
			} else
				log.debug("Rule {} does not match", rule);
		}

		return event;
	}

	protected boolean matches(EventRule<E> rule, E event, RuleContext context)
			throws Exception {
		return rule.matches(event, context);
	}

	/**
	 * This method executes the actions of a rule for a given event.
	 * 
	 * @param rule
	 * @param event
	 */
	protected void execute(EventRule<E> rule, E event,
			Map<String, Object> context) {
		log.debug("Executing rule actions");
	}
}