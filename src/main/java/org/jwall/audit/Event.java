/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.audit;

import java.io.Serializable;


/**
 * <p>
 * This interface defines methods common to all observable events.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public interface Event 
	extends Serializable
{
	public final static String TIMESTAMP = "TIMESTAMP";
	public final static String TYPE = "TYPE";
	
	
	/**
	 * Returns the time of this event in milliseconds since 1970.
	 * 
	 * @return
	 */
	public Long getTimestamp();
	
	
	public EventType getType();

	
	/**
	 * This method returns the specified aspect for the given event.
	 * 
	 * @param variableName
	 * @return
	 */
	public String get( String variableName );
	
	public void set( String variable, String value );
}