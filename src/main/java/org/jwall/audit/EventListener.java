/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.audit;


/**
 * This interface provides a generic way to define a listener class, which
 * can be notified upon arrival of new events.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 * @param <E> The type of events listened for by the implementing class.
 */
public interface EventListener<E extends Event> {

	/**
	 * This method is called upon arrival of a new event of the given type.
	 * 
	 * @param event
	 */
	public void eventArrived( E event );
}
