/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.audit.server;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import org.jwall.web.audit.AuditEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SyslogReceiver extends Thread {

	static Logger log = LoggerFactory.getLogger( SyslogReceiver.class );
    ServerSocket socket;
    AuditEventListener storage = null;
	List<AuditEventStreamHandler> handlers = new ArrayList<AuditEventStreamHandler>();
	
	public SyslogReceiver( InetAddress addr, Integer port, Class<?> handlerClass ) throws Exception {
		socket = new ServerSocket( port, 100, addr );
		log.info( "Listening for connections on " + addr.getHostAddress() + ":" + port );
	}
	
	
	public void run(){
		
		while( true ){
			
			try {
				Socket connection = socket.accept();
				log.info( "incoming connection from {}:{}", connection.getInetAddress().getHostAddress(), connection.getPort() );
				
				AuditEventStreamHandler handler = createHandler( connection );
				handler.start();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	private AuditEventStreamHandler createHandler( Socket connection ) throws Exception {
		
		AuditEventStreamHandler handler = new AuditEventStreamHandler( this, connection, false );
		if( storage != null )
			handler.setEventStore( storage );
		
		synchronized( handlers ){
			handlers.add( handler );
		}
		
		return handler;
	}

	
	protected void handlerFinished( AuditEventStreamHandler handler ){
		synchronized( handlers ){
			handlers.add( handler );
		}
	}
	
	
	public static void main( String[] args ) throws Exception {
		System.setProperty( "storage.engine", "database" );
		SyslogReceiver r = new SyslogReceiver( InetAddress.getByName( "localhost" ), 10000, null );
		r.run();
	}
}