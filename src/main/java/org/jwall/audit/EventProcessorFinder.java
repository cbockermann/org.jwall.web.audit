/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.audit;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventProcessorPipeline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import stream.runtime.setup.factory.ObjectFactory;

/**
 * <p>
 * This class parses an XML document and checks for any <code>Processor</code>
 * or <code>processor</code> elements. These elements will be instantiated as
 * event processors and deployed in the given pipeline object.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class EventProcessorFinder {

	static Logger log = LoggerFactory.getLogger(EventProcessorFinder.class);

	/**
	 * This method parses the given XML file and deploys any processor nodes
	 * that have been found in the given pipeline.
	 * 
	 * @param procDefs
	 * @param pipeline
	 * @throws Exception
	 */
	public void deployCustomEventProcessors(File procDefs,
			AuditEventProcessorPipeline pipeline) throws Exception {
		deployCustomEventProcessors(new FileInputStream(procDefs), pipeline);
	}

	/**
	 * This method parses an XML document from the given input-stream and
	 * deploys any processor nodes that have been found at the given pipeline
	 * instance.
	 * 
	 * @param processorDefinitions
	 * @param pipeline
	 * @throws Exception
	 */
	public void deployCustomEventProcessors(InputStream processorDefinitions,
			AuditEventProcessorPipeline pipeline) throws Exception {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(processorDefinitions);
		Element root = doc.getDocumentElement();

		deployEventProcessors(root.getElementsByTagName("Processor"), pipeline);
		deployEventProcessors(root.getElementsByTagName("processor"), pipeline);
	}

	/**
	 * This method basically does the <i>real work</i> as it parses the list of
	 * candidate nodes that might be referring to processors and creates objects
	 * from theses that are deployed in the specified pipeline.
	 * 
	 * @param candidateNodes
	 * @param pipeline
	 */
	protected void deployEventProcessors(NodeList candidateNodes,
			AuditEventProcessorPipeline pipeline) {

		if (candidateNodes == null || candidateNodes.getLength() == 0) {
			log.debug("List of candidateNodes is empty: {}", candidateNodes);
			return;
		}

		ObjectFactory of = ObjectFactory.newInstance();
		for (int i = 0; i < candidateNodes.getLength(); i++) {

			Node node = candidateNodes.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE
					&& node.getNodeName().equalsIgnoreCase("processor")) {

				try {
					Map<String, String> params = of.getAttributes(node);

					Double priority = new Double(1000.0d);
					if (params.get("priority") != null)
						priority = new Double(params.get("priority"));

					log.info("Found priority {}", priority);

					Object o = of.create((Element) node);

					if (o instanceof EventProcessor) {
						@SuppressWarnings({ "unchecked" })
						EventProcessor<AuditEvent> eventProcessor = (EventProcessor<AuditEvent>) o;
						log.info(
								"Registering AuditEventProcessor {} with priority {}",
								eventProcessor, priority);
						pipeline.register(priority, eventProcessor);
					}

				} catch (Exception e) {
					log.error("Failed to register processor: {}",
							e.getMessage());
					if (log.isDebugEnabled())
						e.printStackTrace();
				}
			} else {
				log.warn("Skipping node {}", node.getNodeName());
			}
		}
	}
}