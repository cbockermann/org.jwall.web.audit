/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.audit.script.utils;

import java.util.Iterator;
import java.util.ListIterator;

import org.jwall.audit.script.EventList;
import org.jwall.audit.script.ScriptEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <p>
 * This is a simple implementation of an iterator and a list iterator. It is
 * backed by a lazy event list.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class ListIteratorImpl implements Iterator<ScriptEvent>, ListIterator<ScriptEvent> {

	static Logger log = LoggerFactory.getLogger( ListIteratorImpl.class );
	EventList<?> list;
	int cur = 0;
	int max = 0;
	
	
	public ListIteratorImpl( EventList<?> list ){
		this( list, 0 );
	}
	
	
	public ListIteratorImpl( EventList<?> list, int offset ){
		this.list = list;
		max = this.list.size();
	}
	
	
	@Override
	public void add(ScriptEvent arg0) {
	}

	
	@Override
	public boolean hasNext() {
		return cur < max;
	}

	@Override
	public boolean hasPrevious() {
		return cur > 0;
	}

	@Override
	public ScriptEvent next() {
		log.debug( "returning next element, cur: {}, max: {}", cur, max );
		if( cur < max )
			return list.get( cur++ );
		return null;
	}

	@Override
	public int nextIndex() {
		return cur;
	}

	@Override
	public ScriptEvent previous() {
		return list.get( --cur );
	}

	@Override
	public int previousIndex() {
		return cur - 1;
	}

	@Override
	public void remove() {
	}

	@Override
	public void set(ScriptEvent arg0) {
	}
}