/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.audit.script.utils;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>
 * This is a very simple counter class that is provided to all scripts
 * and can be used to track the count of event properties when iterating
 * over multiple events.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class Counter {
	
	Map<String,Double> counts = new LinkedHashMap<String,Double>();
	
	
	public void reset(){
		counts.clear();
	}
	
	public void count( String value ){
		Double cnt = counts.get( value );
		if( cnt == null ){
			cnt = 1.0d;
		} else {
			cnt = cnt + 1.0d;
		}
		counts.put( value, cnt );
	}
	
	public Double get( String value ){
		Double cnt = counts.get( value );
		if( cnt == null )
			return 0.0d;
		return cnt;
	}
	
	public void set( String value, Double count ){
		counts.put( value, count );
	}
	
	public Double getTotal(){
		Double total = 0.0d;
		for( Double d : counts.values() )
			total += d;
		
		return total;
	}
	
	public Double sum( Collection<String> keys ){
		Double sum = 0.0d;
		for( String key : keys )
			sum += get( key );
		
		return sum;
	}
}