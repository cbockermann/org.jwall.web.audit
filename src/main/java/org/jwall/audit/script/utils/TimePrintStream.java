/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.audit.script.utils;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

public class TimePrintStream extends PrintStream {

	// flag for start-of-line
	boolean sol = true;
	
	public TimePrintStream(OutputStream out) {
		super(out);
	}

	public TimePrintStream( File file ) throws IOException {
		super( file );
	}
	
	

	@Override
	public void print(Object obj) {
		if( sol )
			printTime();
		super.print(obj);
	}

	@Override
	public void print(String s) {
		if( sol )
			printTime();
		super.print(s);
		sol = false;
	}

	@Override
	public void println() {
		super.println();
		sol = true;
	}

	@Override
	public void println(Object x) {
		printTime();
		super.println(x);
	}


	@Override
	public void println(String x) {
		printTime();
		super.println(x);
	}
	
	protected void printTime(){
		super.print( "@" );
		super.print( System.currentTimeMillis() );
		super.print( ": " );
	}

	@Override
	public void write(int b) {

		if( sol ){
			sol = false;
			printTime();
		}
		
		super.write(b);
		sol = b == '\n';
	}
}
