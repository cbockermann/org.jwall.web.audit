/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.audit.script;

import java.io.Serializable;
import java.net.URL;
import java.util.Map;


/**
 * This interface defines the properties of a script definition. A script
 * needs to provide some containers for results, the script source code and
 * optionally some parameters.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public interface Script extends Serializable {

	/**
	 * This method provides the source code of the script.
	 * 
	 * @return
	 */
	public String getScriptSource();
	
	
	/**
	 * This method determines the language which is used to
	 * interpret/run the script source code.
	 * 
	 * @return
	 */
	public String getScriptLanguage();
	
	
	public void setResultUrl( URL url );
	
	/**
	 * This method returns the ResultUrl of that script. Usually this
	 * value is determined by the script executor and provided to the
	 * implementing class.
	 * 
	 * @return
	 */
	public URL getResultUrl();
	
	
	/**
	 * This method returns a Map of parameters. These can be used as "args"
	 * for the script execution and will be provided to the script using the
	 * <code>$args</code> map.
	 * 
	 * @return
	 */
	public Map<String,Serializable> getParameters();

	
	/**
	 * This returns the result of the script (if any). The script has a
	 * map called <code>$results</code> bound that can be used to add result
	 * data.
	 * 
	 * @return
	 */
	public Map<String,Serializable> getResults();
}