/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.audit.script;

import java.util.ArrayList;
import java.util.List;

import org.jwall.audit.EventType;
import org.jwall.log.LogMessage;
import org.jwall.web.audit.ModSecurity;

public final class ScriptEvent implements org.jwall.audit.Event {

	/** The unique class ID */
	private static final long serialVersionUID = 371392341967963638L;

	org.jwall.audit.Event e;

	public ScriptEvent(org.jwall.audit.Event evt) {
		this.e = evt;
	}

	public boolean isSet(String var) {
		return e.get(var) != null;
	}

	public String get(String var) {
		return e.get(var);
	}

	public List<String> getAll(String var) {

		if (e instanceof org.jwall.web.audit.AuditEvent) {
			return ((org.jwall.web.audit.AuditEvent) e).getAll(var);
		}

		List<String> vals = new ArrayList<String>(1);
		vals.add(e.get(var) + "");
		return vals;
	}

	@Override
	public Long getTimestamp() {
		return e.getTimestamp();
	}

	@Override
	public EventType getType() {
		return e.getType();
	}

	@Override
	public void set(String variable, String value) {
		e.set(variable, value);
	}

	public String toString() {
		if (e instanceof org.jwall.web.audit.AuditEvent)
			return "ScriptEvent[" + e.get(ModSecurity.TX_ID) + "]";

		if (e instanceof org.jwall.log.LogMessage)
			return "LogMessage[" + e.get(LogMessage.MESSAGE) + "]";

		return "ScriptEvent[@" + e.getTimestamp() + "]";
	}
}