/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.log;

import java.io.Serializable;

import org.jwall.audit.Event;



/**
 * <p>
 * This interface defines a simple abstract log message entity, comprised
 * of a timestamp, a source-identifier and the date itself.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 */
public interface LogMessage extends Serializable, Event {

	public final static String SOURCE = "SOURCE";
	public final static String MESSAGE = "MESSAGE";
	public final static String LOG_LEVEL = "LOG_LEVEL";
	
	/**
	 * @return the source
	 */
	public abstract String getSource();

	/**
	 * @return the message
	 */
	public abstract String getMessage();
	
	
	public String get( String variable );
	
	public void set( String variable, String value );
}