/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.log.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Comparator;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeSet;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.util.WildcardPattern;

/**
 * <p>
 * This class implements an input stream that will read from a sequence of
 * files. It is intended to provide the opposite of a split of files into parts.
 * Each part is read in chronological order.
 * </p>
 * <p>
 * At the end, the stream waits for new data to be appended to the last file, or
 * a new file to be created that matches the names.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class SequentialFileInputStream extends InputStream {
	/* A global logger for this class */
	static Logger log = LoggerFactory
			.getLogger(SequentialFileInputStream.class);

	/*
	 * This constant object implements a comparator for files, which determines
	 * the order in which this file stream processes files
	 */
	final static Comparator<File> FILE_ORDER = new Comparator<File>() {
		public int compare(File arg0, File arg1) {
			if (arg0 == arg1 || arg0.compareTo(arg1) == 0)
				return 0;

			Long lastModified = new Long(arg0.lastModified());
			int rc = lastModified.compareTo(arg1.lastModified());
			if (rc == 0)
				return arg0.getName().compareTo(arg1.getName());

			return rc;
		}
	};

	/* The time gap this stream puts itself to sleep until new data is available */
	Integer sleep = 500;

	/* The initial file this reader was created from */
	File file = null;

	/* The file we currently read from */
	File current = null;

	/* The next file to read */
	File next = null;

	/* The input-stream of the current file */
	InputStream reader;

	String pattern = "";

	boolean closed = false;

	long maxWaitingTime = -1L;
	long read = 0L;
	long total = 0L;
	long waitingTime = 0L;
	boolean removeAfterRead = true;
	Set<File> finished = new TreeSet<File>(FILE_ORDER);

	final LinkedBlockingQueue<File> sequence = new LinkedBlockingQueue<File>();

	/**
	 * Creates a new SequentialInputStream, which will read the specified file
	 * and any subsequent files that match the file's name, possibly and a
	 * appended number, i.e. for the file <code>/tmp/test.log</code> the stream
	 * will read
	 * 
	 * <pre>
	 *    /tmp/test.log
	 *    /tmp/test.log.1
	 *    /tmp/test.log.2
	 *    ...
	 * </pre>
	 * 
	 * The trailing digits may as well be time-stamps or the like. The files are
	 * read in order of their last-modification-time.
	 * 
	 * @param file
	 *            The initial file.
	 * @throws IOException
	 */
	public SequentialFileInputStream(File file) throws IOException {
		this(file, false);
	}

	/**
	 * <p>
	 * This creates a SequentialInputStream which will remove any files that
	 * have completeley been read (i.e. they have been processed until EOF
	 * <b>and</b> another, newer file matching the pattern does exist).
	 * </p>
	 * <p>
	 * Whether the old files are to be removed is determined by the
	 * <code>removeAfterRead</code> flag.
	 * </p>
	 * 
	 * @param file
	 *            The initial file to start with.
	 * @param removeAfterRead
	 *            Whether the old files should be removed or not.
	 * @throws IOException
	 */
	public SequentialFileInputStream(File file, boolean removeAfterRead)
			throws IOException {
		this(file, file.getName() + "*", removeAfterRead);
	}

	public SequentialFileInputStream(final File file, final String pattern,
			boolean removeAfterRead) throws IOException {
		this.file = file;
		this.current = this.file;
		this.pattern = pattern;
		this.removeAfterRead = removeAfterRead;

		this.monitorDirectory(file.getParentFile(), pattern);

		TimerTask watcher = new TimerTask() {
			public void run() {
				monitorDirectory(file.getParentFile(), pattern);
			}
		};

		Timer timer = new Timer();
		timer.scheduleAtFixedRate(watcher, 0L, 1000L);
	}

	protected void monitorDirectory(File dir, String pattern) {

		log.debug("Creating sequence from directory '{}', pattern '{}'", dir,
				pattern);
		TreeSet<File> files = new TreeSet<File>(FILE_ORDER);

		File[] fs = dir.listFiles();
		if (fs != null) {

			for (File f : fs) {
				if (WildcardPattern.matches(pattern, f.getName())) {
					if (current.equals(f) || finished.contains(f)) {
						log.debug(
								"Skipping file {} which is already finished.",
								f);
					} else {
						log.debug("Adding new file {}", f);
						files.add(f);
					}
				}
			}
		}

		for (File f : files) {
			if (!sequence.contains(f)) {
				log.debug("Adding file {} to sequence...", f);
				sequence.add(f);
			}
		}
	}

	public boolean matchesSequence(File f) {

		if (f.getAbsolutePath().equals(file.getAbsolutePath()))
			return false;

		if (f.isFile() && !f.equals(file) && f.getName().matches(pattern)) {
			return !finished.contains(f);
		}
		return false;
	}

	public File getCurrentFile() {
		return current;
	}

	/**
	 * This method checks if there exists a next file in the sequence.
	 * 
	 * @return <code>true</code> if a new file exists, which may indicate that
	 *         the current file is finished.
	 */
	protected boolean hasNext() {
		return true;
		// return !sequence.isEmpty();
		//
		// File dir = file.getParentFile();
		// for (File f : dir.listFiles())
		// if (matchesSequence(f) && !f.equals(current))
		// return true;
		// return false;
	}

	/**
	 * <p>
	 * This method closes the current file an opens the next file in the
	 * sequence. If no <i>next</i> file exists, this method will block until one
	 * has been created.
	 * </p>
	 * 
	 * @throws IOException
	 */
	protected void openNextFile() throws IOException {
		log.debug("Current file {} seems to have ended, checking for next one",
				current);
		boolean proceeded = false;

		do {

			try {
				if (current != null) {
					log.debug("Marking file '{}' as finished...", current);
					finished.add(current);
				}

				// log.info("sequence has {} files pending...",
				// sequence.size());
				File file = sequence.take();

				if (file != null) {
					log.debug("Proceeding with file {}", file);
					current = file;
					read = 0L;
					reader = new FileInputStream(current);
					return;
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//
			// TreeSet<File> files = new TreeSet<File>(FILE_ORDER);
			// File dir = file.getParentFile();
			// log.debug("Checking for next file in {}", dir);
			// File[] fs = dir.listFiles();
			// log.debug("Files: {}", (Object[]) fs);
			//
			// if (fs != null) {
			// for (File f : fs) {
			// if (matchesSequence(f) && !finished.contains(f)) {
			// // log.info(
			// // "  File {} is considered a candidate to proceed", f
			// // );
			// files.add(f);
			// }
			// }
			// } else {
			// log.error("Directory {} does not exist!", dir);
			// try {
			// Thread.sleep(1000);
			// } catch (Exception e) {
			// e.printStackTrace();
			// }
			// }
			//
			// SortedSet<File> sequence = files; // .tailSet( current,
			// // finished.contains( current )
			// // );
			// for (File file : sequence) {
			// log.debug("   file: {} (modified: {})", file,
			// file.lastModified());
			// }
			// if (!sequence.isEmpty()) {
			// if (reader != null) {
			// log.debug("Closing old reader on file {}...", current);
			// reader.close();
			// }
			// if (removeAfterRead) {
			// log.debug("Removing file {}", current);
			// current.delete();
			// }
			// finished.add(current);
			// log.debug("Read {} bytes from {}", read, current);
			// current = sequence.first();
			// read = 0L;
			// reader = new FileInputStream(current);
			// log.debug("Now reading from '{}'", current);
			// proceeded = true;
			// } else {
			// try {
			// log.debug("After reading {} bytes from {}", read, current);
			// log.debug("   a total of {} bytes read so far", total);
			// log.debug(
			// "No sequential file found for {}, sleeping for {} ms and checking again...",
			// file, sleep);
			// Thread.sleep(sleep);
			// } catch (Exception e) {
			// }
			// }
		} while (!proceeded);
	}

	/**
	 * <p>
	 * This read method is basically a read of the current open file. It will
	 * block if there is no more data and no new file exists.
	 * </p>
	 * 
	 * @see java.io.InputStream#read()
	 */
	@Override
	public int read() throws IOException {

		if (closed)
			return -1;

		while (reader == null)
			openNextFile();

		int data = reader.read();
		if (data == -1) {
			log.info("File {} seems to have finished!", current);
		}

		while (data == -1) {

			openNextFile();

			data = reader.read();
			if (data == -1) {
				log.info("Opened next file '{}' but still reading '-1'!?",
						current);
			}
			//
			// if (hasNext()) {
			// openNextFile();
			// } else {
			// try {
			// log.info("Waiting for new data to arrive at file {}",
			// current);
			// Thread.sleep(sleep);
			// waitingTime += sleep;
			// } catch (Exception e) {
			// }
			// if (maxWaitingTime > 0 && waitingTime > maxWaitingTime) {
			// closed = true;
			// log.info("Total sleeping time exhausted!");
			// return -1;
			// }
			// }
			// data = reader.read();
			// if (data == -1) {
			// log.info("weird");
			// }
		}
		read++;
		total++;
		return data;
	}

	/**
	 * @see java.io.InputStream#markSupported()
	 */
	@Override
	public boolean markSupported() {
		return false;
	}

	/**
	 * @see java.io.InputStream#available()
	 */
	@Override
	public int available() throws IOException {
		if (reader == null)
			return super.available();
		return reader.available();
	}

	public static void main(String[] args) throws Exception {
		File input = new File("/tmp/test.log");

		SequentialFileInputStream f = new SequentialFileInputStream(input,
				false);
		PrintStream out = new PrintStream(new FileOutputStream(new File(
				input.getAbsolutePath() + "-complete")));

		byte[] buf = new byte[1024];
		int read = 0;
		int written = 0;
		do {
			if (f.available() > 0)
				buf = new byte[Math.min(1024, f.available())];
			read = f.read(buf);
			if (read >= 0) {
				written += read;
				out.write(buf, 0, read);
				out.flush();
			}
			log.debug("{} bytes written", written);
		} while (read > 0);
	}
}