/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.log.io;

import java.io.IOException;
import java.util.Map;

import org.jwall.log.LogMessage;

/**
 * This interface defines the methods provided by all readers which provide
 * access to parsing log-messages from files.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 */
public interface LogReader {

	/**
	 * Reads the next LogMessage. Will return <code>null</code> if no more messages
	 * can be read.
	 * 
	 * @return
	 * @throws IOException
	 */
	public LogMessage readNext() throws IOException;
	
	
	/**
	 * This sets a number of default properties, which will be given to all messages
	 * created by the implementing reader. The defaults may be overwritten by more
	 * message-specific values during parsing.
	 * 
	 * @param defaults
	 */
	public void setDefaults( Map<String,String> defaults );
}
