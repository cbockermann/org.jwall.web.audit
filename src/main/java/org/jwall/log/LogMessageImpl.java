/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.log;

import java.util.HashMap;
import java.util.Map;

import org.jwall.audit.EventType;




/**
 * A simple implementation of the LogMessage interface.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class LogMessageImpl implements LogMessage {

	/** The unique class ID */
	private static final long serialVersionUID = 1118705739440576195L;
	
	EventType type;
	Long timestamp;
	String source;
	String msg;
	Map<String,String> vals = new HashMap<String,String>();
	
	public LogMessageImpl( EventType type, Long timestamp, String source, String msg ){
		this.type = type;
		this.timestamp = timestamp;
		this.source = source;
		this.msg = msg;
	}
	
	

	/**
	 * @return the type
	 */
	public EventType getType() {
		return type;
	}



	/**
	 * @see org.jwall.log.LogMessage#getMessage()
	 */
	@Override
	public String getMessage() {
		return msg;
	}

	
	/**
	 * @see org.jwall.log.LogMessage#getSource()
	 */
	@Override
	public String getSource() {
		return source;
	}
	

	/**
	 * @see org.jwall.log.LogMessage#getTimestamp()
	 */
	@Override
	public Long getTimestamp() {
		return timestamp;
	}
	
	public String toString(){
		return msg;
	}


	@Override
	public String get(String variable) {
		if( TIMESTAMP.equals( variable ) )
			return getTimestamp() + "";
	
		return this.vals.get( variable );
	}
	
	public void set( String variable, String val ){
		vals.put( variable, val );
	}
}