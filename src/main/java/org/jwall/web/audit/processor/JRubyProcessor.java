/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.processor;

import java.util.Map;

import org.jwall.audit.EventProcessor;
import org.jwall.web.audit.AuditEvent;

public class JRubyProcessor implements EventProcessor<AuditEvent> {

	String script;
	
	
	public JRubyProcessor(){
	}
	
	
	@Override
	public AuditEvent processEvent(AuditEvent event, Map<String, Object> context) throws Exception {
		
		if( script == null )
			return event;
		
		return event;
	}
}