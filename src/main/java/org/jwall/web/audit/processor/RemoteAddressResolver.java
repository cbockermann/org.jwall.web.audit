/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.processor;

import java.util.Map;

import org.jwall.audit.EventProcessor;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * This processor extracts a possible original remote IP from the Http request
 * header and places it into the REMOTE_ADDR variable. This may be useful for
 * load-balancing setups, which mangle the real IP address of the remote client.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class RemoteAddressResolver implements EventProcessor<AuditEvent> {

	static Logger log = LoggerFactory.getLogger(RemoteAddressResolver.class);

	String from = "REQUEST_HEADERS:X-Orig-Source-IP".toUpperCase();

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * @see org.jwall.audit.EventProcessor#processEvent(org.jwall.audit.Event,
	 *      java.util.Map)
	 */
	@Override
	public AuditEvent processEvent(AuditEvent event, Map<String, Object> context)
			throws Exception {

		String txId = event.get(ModSecurity.TX_ID);
		log.debug("Trying to resolve true remote-address for {}", txId);

		String addr = event.get(from);
		log.debug("Extracted address value is: '{}'", addr);

		if (addr != null && !"".equals(addr.trim())) {
			addr = addr.trim();

			String remote = event.get(ModSecurity.REMOTE_ADDR);
			log.info("Replacing REMOTE_ADDR '{}' with '{}'", remote, addr);
			event.set(ModSecurity.REMOTE_ADDR, addr);
		} else {
			log.debug(
					"No original address could be extracted from '{}' for event {}",
					from, txId);
		}

		return event;
	}
}
