/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.processor;

import java.util.List;
import java.util.Map;

import org.jwall.audit.EventProcessor;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventMessage;
import org.jwall.web.audit.ModSecurity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Keys;
import stream.annotations.Description;

/**
 * This process simply parses the EventMessages and extracts all RULE_TAGS to
 * add them to the TAGS attribute.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
@Description(group = "AuditEvent.Processing", text = "This processor adds all rule-tags as tags for the event.")
public class RuleTagProcessor implements EventProcessor<AuditEvent> {
	static Logger log = LoggerFactory.getLogger(RuleTagProcessor.class);

	String separator = "|";

	String user = "system";

	String[] tags;

	/**
	 * @return the separator
	 */
	public String getSeparator() {
		return separator;
	}

	/**
	 * @param separator
	 *            the separator to set
	 */
	public void setSeparator(String separator) {
		this.separator = separator;
	}

	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * @see org.jwall.audit.EventProcessor#processEvent(org.jwall.audit.Event,
	 *      java.util.Map)
	 */
	@Override
	public AuditEvent processEvent(AuditEvent event, Map<String, Object> context) throws Exception {
		log.debug("adding RuleTags to event {}", event.get(ModSecurity.TX_ID));

		// the tag-string
		//
		StringBuilder s = new StringBuilder();
		String existingTags = event.get(AuditEvent.TAGS);
		if (existingTags != null) {
			s.append(existingTags);
		}

		int added = 0;
		AuditEventMessage[] msgs = event.getEventMessages();
		log.debug("EventMessages: {}", (Object[]) msgs);
		if (msgs == null) {
			return event;
		}

		for (AuditEventMessage msg : msgs) {

			log.debug("Processing message: {}", msg);

			//
			// iterate over the rule-tags
			//
			List<String> tags = msg.getRuleTags();
			if (tags == null) {
				continue;
			}

			for (String tag : tags) {

				if (this.tags != null && !Keys.isSelected(tag, this.tags)) {
					log.debug("Ignoring TAG '{}' due to filter!");
					continue;
				}

				if (s.length() > 0) {
					s.append(separator);
				}

				s.append(user);
				s.append(":");
				s.append(tag);
				added++;
			}
		}

		if (added > 0) {

			if (s.length() > 0)
				s.append(separator);

			String tagString = s.toString();
			log.debug("Adding final tag-string: '{}'", tagString);
			event.set(AuditEvent.TAGS, tagString);
		}

		return event;
	}

	public String[] getTags() {
		return tags;
	}

	public void setTags(String[] tags) {
		this.tags = tags;
	}
}