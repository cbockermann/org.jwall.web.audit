/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit;

/**
 * @author chris
 * 
 */
public class VariableMapping {

	public static String[] CASE_INSENSITIVE_PREFIXES = new String[] {
			"REQUEST_HEADERS:", "RESPONSE_HEADERS:" };

	public String map(String variable) {
		return variable;
	}

	public static boolean isCaseInsensitive(String var) {

		for (String prefix : CASE_INSENSITIVE_PREFIXES) {
			if (var.toUpperCase().startsWith(prefix)) {
				return true;
			}
		}

		return false;
	}
}
