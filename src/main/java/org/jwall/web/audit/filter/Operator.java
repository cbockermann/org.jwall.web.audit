/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.filter;

import java.io.Serializable;

import org.jwall.web.audit.SyntaxException;

/**
 * <p>
 * An enumeration of available operators.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public enum Operator implements Serializable {

	EQ("@eq"), NEQ("!@eq"), GE("@ge"), NGE("!@ge"), GT("@gt"), NGT("!@gt"), LE(
			"@le"), NLE("!@le"), LT("@lt"), NLT("!@lt"), PM("@pm"), NPM("!@pm"), RX(
			"@rx"), NRX("!@rx"), SX("@sx"), NSX("!@sx"), IN("@in"), NIN("!@in"), Contains(
			"@contains"), NContains("!@contains"), BeginsWith("@beginsWith"), NBeginsWith(
			"!@beginsWith"), EndsWith("@endsWith"), NEndsWith("!@endsWith");

	private final String name;

	Operator(String str) {
		this.name = str;
	}

	public String toString() {
		return name;
	}

	public static Operator read(String str) throws SyntaxException {

		for (Operator op : values())
			if (op.name.equals(str))
				return op;

		if ("=".equals(str) || "==".equals(str))
			return EQ;

		if ("!=".equals(str) || "<>".equals(str))
			return NEQ;

		if ("<=".equals(str))
			return LE;

		if ("<".equals(str))
			return LT;

		if (">=".equals(str))
			return GE;

		if (">".equals(str))
			return GT;

		throw new FilterException("Invalid operator name: '" + str + "'!");
	}
}