/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.filter;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.jwall.web.audit.AuditEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Implements a complex expression which ORs or ANDs multiple single filter
 * expressions into one.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 */
public class FilterExpressionList implements FilterExpression {
	/** The unique class ID */
	private static final long serialVersionUID = -6592861898522001021L;

	static Logger log = LoggerFactory.getLogger(FilterExpressionList.class);

	BooleanOperator op;
	Collection<FilterExpression> exps;

	public FilterExpressionList(BooleanOperator op,
			Collection<FilterExpression> exps) {
		this.op = op;
		this.exps = exps;
	}

	public FilterExpressionList(BooleanOperator op,
			List<AuditEventMatch> matches) {
		this.op = op;
		exps = new LinkedList<FilterExpression>();
		for (AuditEventMatch m : matches)
			exps.add(m);
	}

	public FilterExpressionList(AuditEventFilter filter) {
		this(BooleanOperator.AND, new LinkedList<FilterExpression>());
		for (AuditEventMatch m : filter.getMatches())
			exps.add(m);
	}

	public BooleanOperator getOperator() {
		return op;
	}

	/**
	 * @see org.jwall.web.audit.filter.FilterExpression#matches(org.jwall.web.audit.AuditEvent)
	 */
	@Override
	public boolean matches(AuditEvent evt) {
		switch (op) {
		case OR:
			return or(evt);
		default:
			return and(evt);
		}
	}

	private boolean and(AuditEvent evt) {
		log.debug("Asserting all matches!");
		for (FilterExpression exp : exps) {
			if (!exp.matches(evt))
				return false;
		}

		return true;
	}

	private boolean or(AuditEvent evt) {
		log.debug("Asserting any match!");

		for (FilterExpression exp : exps) {
			if (exp.matches(evt))
				return true;
		}

		return false;
	}

	public int size() {
		return exps.size();
	}

	public Collection<FilterExpression> getElements() {
		return exps;
	}

	public FilterExpression getFirst() {
		if (exps.isEmpty())
			return null;
		return exps.iterator().next();
	}

	public String toString() {
		StringBuffer s = new StringBuffer("");
		if (exps.size() > 1)
			s.append("( ");

		for (FilterExpression e : exps) {
			if (s.length() > 2)
				s.append("  " + op + "  ");
			s.append(FilterCompiler.toFilterString(e));
		}
		if (exps.size() > 1)
			s.append(" )");
		return s.toString();
	}
}