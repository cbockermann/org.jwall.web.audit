/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;


/**
 * 
 * This simple writer creates an audit-log file in the format of the ModSecurity2 audit-log.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class ModSecurity2AuditWriter
implements AuditEventWriter
{
	/** The writer used for writing to a file */
	private PrintWriter writer;

	
	/**
	 * 
	 *
	 */
	public ModSecurity2AuditWriter(File file)
		throws IOException
	{
		if( file.isDirectory() )
			throw new IOException( "Cannot write to a directory!" );

		if( file.exists() && !file.canWrite() )
			throw new IOException( "Cannot open file "+file.getAbsolutePath()+" for writing!" );

		if(! file.exists() && !file.createNewFile() )
			throw new IOException( "Cannot create file "+file.getAbsolutePath() + "!");
		
		writer = new PrintWriter( new FileWriter( file ) );
	}


	/**
	 * 
	 * Simply append an audit-event entry to the file.
	 * 
	 * @param evt The audit-event to be written.
	 * @throws IOException In case an IO-error occurs.
	 * 
	 */
	public void writeEvent(AuditEvent evt) throws IOException {

		for(int i = 0; i < ModSecurity.SECTIONS.length(); i++){
			char sec = ModSecurity.SECTIONS.charAt(i);
			String section = evt.getSection(i);
			if( section != null && ! "".equals( section.trim() ) ){
				writer.println("--"+evt.getEventId()+"-"+sec+"--");
				writer.print( section );
			}
		}

		writer.println("--"+evt.getEventId()+"-Z--");

		writer.flush();
		
	}
	
	public void close(){
	    writer.close();
	}
}
