/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.io;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.jwall.web.audit.AuditEvent;

import stream.Data;
import stream.data.DataFactory;
import stream.io.AbstractStream;
import stream.io.SourceURL;

/**
 * This class implements a data stream in the context of the streams library.
 * The stream parses ModSecurity audit-log data.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class ModSecurityAuditStream extends AbstractStream {

	URL url;
	InputStream input;
	ModSecurity2AuditReader reader;

	public ModSecurityAuditStream(URL url) throws IOException {
		this(url.openStream());
		this.url = url;
	}

	public ModSecurityAuditStream(SourceURL url) throws Exception {
		super(url);
	}

	public ModSecurityAuditStream(InputStream in) throws IOException {
		input = in;
		reader = new ModSecurity2AuditReader(input);
	}

	@Override
	public void close() throws Exception {
		reader.close();
	}

	@Override
	public void init() throws Exception {
		super.init();

		if (input == null) {
			input = this.getInputStream();
		}

		reader = new ModSecurity2AuditReader(input);
	}

	@Override
	public Data readNext() throws Exception {

		AuditEvent evt = reader.readNext();
		if (evt == null)
			return null;

		Data item = DataFactory.create();
		item.put("event", evt);
		return item;
		// return new AuditData(evt);
	}
}
