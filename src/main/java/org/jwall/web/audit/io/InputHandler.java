/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.io;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;

public class InputHandler 
    extends Thread 
{
    static Integer id = 0;
    
    Integer myId = -1;
    Socket socket;
    
    public InputHandler( Socket sock ){
        this.socket = sock;
        
        synchronized( id ){
            myId = id++;
        }
    }

    
    public void run(){
        
        try {
            BufferedReader r = new BufferedReader( new InputStreamReader( socket.getInputStream() ) );
            
            while( true ){
                
                String line = r.readLine();
                
                while( line == null ){
                    Thread.sleep( 500 );
                    line = r.readLine();
                }
                
                System.out.println( "InputHandler[" + myId + "]: " + line );
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }   
}
