/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.io;

import java.util.concurrent.LinkedBlockingQueue;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.filter.AuditEventFilter;


/**
 * 
 * This is an implementation of a buffered event-source. Events
 * are read in a separate thread an stored in memory until they
 * are fetched by the <code>nextEvent</code> method.
 * 
 * The buffer size is given at creation time.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class BufferedAuditEventSource
extends Thread
implements AuditEventSource 
{
	/* The queue which holds the events */
	private LinkedBlockingQueue<AuditEvent> events;

	/* The reader which provides events one after another ;-) */
	private AuditEventReader reader;
	
	public BufferedAuditEventSource( AuditEventReader evtReader ){
		this( evtReader, 1000 );
	}
	
	
	/**
	 * This creates a buffered source that fetches events from the
	 * reader given as <code>evtReader</code> and stores up to 
	 * <code>bufSize</code> events in memory. If the internal
	 * event-queue is full, it will block.
	 * 
	 * @param evtReader
	 * @param bufSize
	 */
	public BufferedAuditEventSource( AuditEventReader evtReader, int bufSize ){
		reader = evtReader;
		events = new LinkedBlockingQueue<AuditEvent>(bufSize);
	}

	
	/**
	 * @see org.jwall.web.audit.io.AuditEventSource#hasNext()
	 */
	public boolean hasNext() {
		return ! events.isEmpty();
	}

	
	/**
	 * 
	 * @see org.jwall.web.audit.io.AuditEventSource#nextEvent()
	 */
	public AuditEvent nextEvent() {
		return events.poll();
	}

	
	/**
	 * 
	 * @see org.jwall.web.audit.io.AuditEventSource#setFilter(org.jwall.web.audit.AuditEventFilter)
	 */
	public void setFilter(AuditEventFilter filter) {
	}

	
	/**
	 * This method loops forever, waiting for new events to arrive.
	 * 
	 * @see java.lang.Thread#run()
	 */
	public void run(){

		try {
			while( true ){

				AuditEvent evt = null; 

				while( events.remainingCapacity() ==  0 )
					Thread.sleep( 256 );
					
				while( events.remainingCapacity() >  0 ){

					evt = reader.readNext();
					if( evt != null ){
						events.add( evt );
					} else {

						Thread.sleep( 256 );

					}
					
				}
			}
		} catch (Exception e){
			e.printStackTrace();
		}
	}
}
