/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.jwall.web.audit.AuditEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * This class defines several constants for AuditLog file formats. In addition
 * to that it provides a simple static guess-method for a best-effort guess of a
 * file or input-stream. This will result in either a code for a log-format or
 * -1 if no proper format has been found.
 * 
 * @author Christian Bockerman &lt;chris@jwall.org&gt;
 */
public abstract class AuditFormat {
	public final static int UNKNOWN_FORMAT = -1;
	public final static int APACHE_ACCESS_LOG = 0;
	public final static int MOD_SECURITY_1_X_SERIAL_LOG = 1;
	public final static int MOD_SECURITY_2_X_SERIAL_LOG = 2;
	public final static int MOD_SECURITY_2_X_CONCURRENT_LOG = 3;
	public final static int IRONBEE_AUDIT_LOG = 4;

	private static Logger log = LoggerFactory.getLogger("AuditFormat");

	/** This array holds names of the known formats */
	public final static String[] FORMAT_NAMES = { "Access Log",
			"ModSecurity 1.x Serial Audit-Log",
			"ModSecurity 2.x Serial Audit-Log",
			"ModSecurity 2.x Concurrent Audit-Log", "IronBee Audit-Log" };

	public static int guessFormat(File inFile) throws IOException {
		return guessFormat(new FileInputStream(inFile));
	}

	/**
	 * Return the most probable audit-format for the given input-string.
	 * 
	 * @param in
	 *            The input-stream to guess the audit-format from.
	 * @return The code of the input-format that was found to be matching.
	 */
	public static int guessFormat(InputStream in) throws IOException {
		BufferedReader r = new BufferedReader(new InputStreamReader(in));

		try {
			if (isConcurrent2xLog(r)) {
				r.close();
				return MOD_SECURITY_2_X_CONCURRENT_LOG;
			}
		} catch (Exception e) {
			log.debug("Failed to parse ModSecurity 2.x Concurrent-Log, probably file in different Format");
		}

		try {
			if (isAccessLog(r)) {
				r.close();
				return APACHE_ACCESS_LOG;
			}
		} catch (Exception e) {
			log.debug("Failed to parse Apache Access-Log, probably file in different format!");
		}

		try {
			if (isSerial2xLog(r)) {
				r.close();
				return MOD_SECURITY_2_X_SERIAL_LOG;
			}
		} catch (Exception e) {
			log.debug("Failed to parse ModSecurity 2.x Serial-Log, probably file in different format!");
		}

		try {
			if (isSerial1xLog(r)) {
				r.close();
				return MOD_SECURITY_1_X_SERIAL_LOG;
			}
		} catch (Exception e) {
			log.debug("Failed to parse ModSecurity 1.x Serial-Log, probably file in different format!");
		}

		r.close();
		return UNKNOWN_FORMAT;
	}

	/**
	 * This method checks whether the reader is attached to some standard
	 * access-log input. Currently there is no auto-detection of customized
	 * access-log formats, i.e. this method will return <code>true</code> only
	 * if the lines read from the reader are matching Apache's standard
	 * access-logs.
	 * 
	 * @param r
	 *            The reader to read input data from.
	 * @return <code>true</code>, if the lines read from the reader are in
	 *         standard access-log format.
	 */
	public static boolean isAccessLog(BufferedReader r) throws IOException {

		if (r.markSupported())
			r.mark(4096);

		try {
			String line = r.readLine();
			if (line != null) {
				AuditEvent evt = AccessLogAuditReader.createEvent(line);
				if (evt != null) {
					if (r.markSupported())
						r.reset();
					return true;
				}
			}
		} catch (Exception e) {
			if (r.markSupported())
				r.reset();
			return false;
		}
		r.reset();
		return false;
	}

	/**
	 * This method tries to determine whether the reader produces lines in the
	 * format of the ModSecurity 2.x concurrent audit-log files. The reader is
	 * supposed to read the index-file of the concurrent audit-log setup.
	 * 
	 * @param r
	 *            The reader used to read audit-data from the index-file source.
	 * @return <code>true</code> if the audit-log data could be parsed using the
	 *         ModSecurity 2.x index audit-format.
	 * @throws IOException
	 *             In case an I/O (read) error occurred.
	 */
	public static boolean isConcurrent2xLog(BufferedReader r)
			throws IOException {
		r.mark(4096);
		int retry = 2;

		while (retry-- > 0) {
			String line = r.readLine();

			log.debug("Checking line: {}", line);

			if (line != null && line.split(" ").length < 3) {
				r.reset();
				return false;
			}

			String token[] = AccessLogAuditReader.splitAccessLine(line);
			if (token.length > 14) {
				String id = token[10];
				String match = "^/\\d*/\\d*-\\d*/\\d*-\\d*-" + id + "$";
				if (token[12].matches(match)) {
					r.reset();
					return true;
				}
			}
		}
		r.reset();
		return false;
	}

	/**
	 * This method tries to determine whether the reader produces lines in the
	 * format of the ModSecurity 1.x serial audit-log files.
	 * 
	 * @param r
	 *            The reader to read audit-data from.
	 * @return <code>true</code> if the audit-log data could be parsed using the
	 *         ModSecurity 1.x audit-format.
	 * @throws IOException
	 *             In case an I/O (read) error occurred.
	 */
	public static boolean isSerial1xLog(BufferedReader r) throws IOException {
		try {
			if (r.markSupported())
				r.mark(4096);

			int retry = 25;
			while (retry-- > 0) {

				String line = r.readLine();
				if (line == null)
					return false;

				if (line.matches("^========================================$")) {
					if (r.markSupported())
						r.reset();
					return true;
				}
			}
			if (r.markSupported())
				r.reset();
		} catch (Exception e) {
			if (log.isDebugEnabled())
				e.printStackTrace();
		}
		return false;
	}

	/**
	 * This method tries to determine whether the reader produces lines in the
	 * format of the ModSecurity 2.x serial audit-log files.
	 * 
	 * @param r
	 *            The reader to read audit-data from.
	 * @return <code>true</code> if the audit-log data could be parsed using the
	 *         ModSecurity 2.x audit-format.
	 * @throws IOException
	 *             In case an I/O (read) error occurred.
	 */
	public static boolean isSerial2xLog(BufferedReader r) throws IOException {

		if (r.markSupported())
			r.mark(4096);

		int retry = 10;
		while (retry-- > 0) {

			String line = r.readLine();
			if (line == null)
				return false;

			if (line.matches("^--[A-Za-z0-9]*-A--$")) {
				if (r.markSupported())
					r.reset();
				return true;
			}
		}

		if (r.markSupported())
			r.reset();
		return false;
	}

	/**
	 * This method creates the appropriate reader for a file, denoted by
	 * <code>filename</code>.
	 * 
	 * @param filename
	 *            The file from which audit-log data is to be read.
	 * @param tail
	 *            Indicates, whether the reader should skip to the last line of
	 *            the file and only read new data which is appended.
	 * @return An instance of the AuditEventReader interface that is able to
	 *         read audit-events from the given file.
	 * @throws IOException
	 *             In case an I/O error occurred when trying open/read the file.
	 * @throws AuditFormatException
	 *             If the audit-format of the file could not be determined.
	 */
	public static AuditEventReader createReader(String filename, boolean tail)
			throws IOException, AuditFormatException {

		AuditEventReader reader = null;

		File logFile = new File(filename);
		int fmt = AuditFormat.guessFormat(logFile);

		if (fmt == AuditFormat.APACHE_ACCESS_LOG)
			reader = new AccessLogAuditReader(logFile, tail);

		if (fmt == AuditFormat.MOD_SECURITY_1_X_SERIAL_LOG)
			reader = new ModSecurityAuditReader(logFile, tail);

		if (fmt == AuditFormat.MOD_SECURITY_2_X_SERIAL_LOG)
			reader = new ModSecurity2AuditReader(logFile, tail);

		if (reader == null)
			throw new AuditFormatException(
					"The log-file format is not supported!");

		return reader;
	}
}
