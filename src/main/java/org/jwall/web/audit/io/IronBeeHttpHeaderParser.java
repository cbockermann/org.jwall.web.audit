/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.io;

import java.io.BufferedReader;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;

public class IronBeeHttpHeaderParser extends IronBeeSectionParser {

	static Logger log = LoggerFactory.getLogger(IronBeeHttpHeaderParser.class);
	String sectionName;
	String collectionName;
	String firstLine;

	public IronBeeHttpHeaderParser(String sectionName, String collectionName,
			String firstLineName) {
		this.sectionName = sectionName;
		this.collectionName = collectionName;
		this.firstLine = firstLineName;
	}

	@Override
	public String getSectionName() {
		return sectionName;
	}

	@Override
	public Data parseSection(BufferedReader reader, Data data)
			throws IOException {

		String line = reader.readLine();
		if (line != null) {
			log.trace("Adding   ( {}, {} )", firstLine, line);
			data.put(firstLine, line);
		}

		line = reader.readLine();
		while (line != null) {

			int idx = line.indexOf(": ");
			if (idx > 0) {

				String header = line.substring(0, idx);
				String value = line.substring(idx + 2);
				String key = collectionName + ":" + header.toUpperCase();
				data.put(key, value);
				log.trace("Adding   ( {}, {} )", key, value);
			}

			line = reader.readLine();
		}

		return data;
	}
}
