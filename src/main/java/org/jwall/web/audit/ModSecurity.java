/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit;

/**
 * <p>
 * This class defines the constants which are used in the whole web-audit
 * library. These define the strings that are used within
 * <code>mod_security2</code>.
 * </p>
 * <p>
 * As all other classes of the web-audit library this class is in no way
 * officially connected to Breach Security.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public abstract class ModSecurity {
	//
	// Definitions of the AuditEvent-sections
	//
	/**
	 * A string holding all characters that refer to a specific section of an
	 * audit-event.
	 */
	public final static String SECTIONS = "ABCDEFGHIJKTXZ";

	/** The index of the audit log header. */
	public final static int SECTION_AUDIT_LOG_HEADER = 0;

	/** The index of the request-header. */
	public final static int SECTION_REQUEST_HEADER = 1;

	/** The index of the request-body. */
	public final static int SECTION_REQUEST_BODY = 2;

	/** The index of the response-body. */
	public final static int SECTION_RESPONSE_BODY = 4;

	/** The index of the final response-header. */
	public final static int SECTION_FINAL_RESPONSE_HEADER = 5;

	/** The index of the audit-log trailer. */
	public final static int SECTION_AUDIT_TRAILER = 7;

	/** The index of the form-data string. */
	public final static int SECTION_FORM_DATA = 8;

	/** This section holds a list of executed rules (since ModSecurity 2.5.x) */
	public final static int SECTION_RULE_LOG = 10;

	public final static int SECTION_TEST = SECTIONS.indexOf("T");

	public final static int SECTION_META_INF = SECTIONS.indexOf("X");

	/** The index of the audit-log footer. This is always the last one. */
	public final static int SECTION_AUDIT_LOG_FOOTER = SECTIONS.length() - 1;

	/** the regular expression that is used to split the uri and query-string */
	public static String ARGUMENT_SEPARATOR = "(\\?|;)"; // This should become
															// configurable from
															// outside

	/**
	 * This is the prefix-string which denotes a collections element count
	 * rather than the collections values
	 */
	public final static String COLLECTION_SIZE_OPERATOR = "&";

	public final static String[] SECTION_NAMES = { "SECTION_AUDIT_LOG_HEADER",
			"SECTION_REQUEST_HEADER", "SECTION_REQUEST_BODY",
			"__UNNAMED_SECTIONS[4]__", "SECTION_RESPONSE_BODY",
			"SECTION_FINAL_RESPONSE_BODY", "__UNNAMED_SECTIONS[6]__",
			"SECTION_AUDIT_TRAILER", "SECTION_AUDIT_FORM_DATA",
			"__UNNAMED_SECTIONS[9]__", "SECTION_RULE_LOG", "SECTION_TEST",
			"SECTION_AUDIT_LOG_FOOTER" };

	public final static String AUDIT_LOG_TRAILER = "__AUDIT_LOG_TRAILER__";

	public final static String __UNDEFINED__ = "__UNDEFINED__";

	//
	// Definitions of the variables accessable by mod_security
	//
	public final static String ARGS = "ARGS";
	public final static String ARGS_NAMES = "ARGS_NAMES";
	public final static String ARGS_GET = "ARGS_GET";
	public final static String ARGS_GET_NAMES = "ARGS_GET_NAMES";
	public final static String ARGS_POST = "ARGS_POST";
	public final static String ARGS_POST_NAMES = "ARGS_POST_NAMES";

	public final static String REMOTE_HOST = "REMOTE_HOST";
	public final static String REMOTE_ADDR = "REMOTE_ADDR";
	public final static String REMOTE_PORT = "REMOTE_PORT";
	public final static String REMOTE_USER = "REMOTE_USER";

	public final static String PATH_INFO = "PATH_INFO";
	public final static String QUERY_STRING = "QUERY_STRING";
	public final static String AUTH_TYPE = "AUTH_TYPE";
	public final static String SERVER_NAME = "SERVER_NAME";
	public final static String SERVER_ADDR = "SERVER_ADDR";
	public final static String SERVER_PORT = "SERVER_PORT";

	public final static String TIME = "TIME";
	public final static String TIME_SEC = "TIME_SEC";
	public final static String TIME_MIN = "TIME_MIN";
	public final static String TIME_HOUR = "TIME_HOUR";
	public final static String TIME_DAY = "TIME_DAY";
	public final static String TIME_WDAY = "TIME_WDAY";
	public final static String TIME_MONTH = "TIME_MONTH";
	public final static String TIME_YEAR = "TIME_YEAR";

	/**
	 * @deprecated Use TX_ID instead!
	 */
	public final static String UNIQUE_ID = "UNIQUE_ID";

	public final static String REQUEST_URI = "REQUEST_URI";
	public final static String REQUEST_URI_RAW = "REQUEST_URI_RAW";
	public final static String REQUEST_PATH = "REQUEST_PATH";
	public final static String REQUEST_LINE = "REQUEST_LINE";
	public final static String REQUEST_METHOD = "REQUEST_METHOD";
	public final static String REQUEST_PROTOCOL = "REQUEST_PROTOCOL";
	public final static String REQUEST_FILENAME = "REQUEST_FILENAME";
	public final static String REQUEST_BASENAME = "REQUEST_BASENAME";

	public final static String SCRIPT_FILENAME = __UNDEFINED__;
	public final static String SCRIPT_BASENAME = __UNDEFINED__;
	public final static String SCRIPT_UID = __UNDEFINED__;
	public final static String SCRIPT_GID = __UNDEFINED__;
	public final static String SCRIPT_USERNAME = __UNDEFINED__;
	public final static String SCRIPT_GROUPNAME = __UNDEFINED__;
	public final static String SCRIPT_MODE = __UNDEFINED__;

	public final static String ENV = "ENV";
	public final static String REQUEST_HEADER = "REQUEST_HEADER";
	public final static String REQUEST_HEADERS = "REQUEST_HEADERS";
	public final static String REQUEST_HEADERS_NAMES = "REQUEST_HEADERS_NAMES";
	public final static String REQUEST_COOKIES = "REQUEST_COOKIES";
	public final static String REQUEST_COOKIES_NAMES = "REQUEST_COOKIES_NAMES";
	public final static String REQUEST_BODY = "REQUEST_BODY";

	public final static String PROTOCOL = REQUEST_PROTOCOL;

	public final static String RESPONSE_LINE = "RESPONSE_LINE";
	public final static String RESPONSE_STATUS = "RESPONSE_STATUS";
	public final static String RESPONSE_PROTOCOL = "RESPONSE_PROTOCOL";
	public final static String RESPONSE_HEADER = "RESPONSE_HEADER";
	public final static String RESPONSE_HEADERS = "RESPONSE_HEADERS";
	public final static String RESPONSE_HEADERS_NAMES = "RESPONSE_HEADERS_NAMES";
	public final static String RESPONSE_BODY = "RESPONSE_BODY";
	public final static String RESPONSE_COOKIES = "RESPONSE_COOKIES";
	public final static String RESPONSE_COOKIES_NAMES = "RESPONS_COOKIES_NAMES";

	public final static String TX = "TX";
	public final static String TX_ID = "TX_ID";
	public final static String SESSION = "SESSION";
	public final static String WEBAPPID = "WEBAPPID";
	public final static String SESSIONID = "SESSIONID";
	public final static String USERID = __UNDEFINED__;

	public final static String GEO = "GEO";

	/* External variables, not included in the real ModSecurity implementation */
	public final static String EVENT_ID = "EVENT_ID";
	public final static String RULE_ID = "RULE_ID";

	public final static String RULE_FILE = "RULE_FILE";
	public final static String RULE_LINE = "RULE_LINE";

	public final static String RULE_MSG = "RULE_MSG";
	public final static String RULE_TAG = "RULE_TAG";
	public final static String RULE_SEV = "RULE_SEV";
	public final static String RULE_DATA = "RULE_DATA";
	public final static String DATE = "DATE";

	public final static String HIGHEST_SEVERITY = "HIGHEST_SEVERITY";
	public final static String LOWEST_SEVERITY = "LOWEST_SEVERITY";

	public final static String PERF_REQ_START = "PERF_REQ_START";
	public final static String PERF_REQ_PROC = "PERF_REQ_PROC";
	public final static String PERF_COMBINED = "PERF_COMBINED";
	public final static String PERF_PHASE1 = "PERF_PHASE1";
	public final static String PERF_PHASE2 = "PERF_PHASE2";
	public final static String PERF_PHASE3 = "PERF_PHASE3";
	public final static String PERF_PHASE4 = "PERF_PHASE4";
	public final static String PERF_PHASE5 = "PERF_PHASE5";
	public final static String PERF_SREAD = "PERF_SREAD";
	public final static String PERF_SWRITE = "PERF_SWRITE";
	public final static String PERF_LOGGING = "PERF_LOGGING";
	public final static String PERF_GC = "PERF_GC";

	/* An array of all implemented variable-names */
	public final static String[] VARIABLES = { EVENT_ID, DATE, UNIQUE_ID, ARGS,
			ARGS_NAMES, REMOTE_HOST, REMOTE_ADDR, REMOTE_PORT, REMOTE_USER,
			PATH_INFO, QUERY_STRING, AUTH_TYPE, SERVER_NAME, SERVER_ADDR,
			SERVER_PORT, REQUEST_URI, REQUEST_URI_RAW, REQUEST_LINE,
			REQUEST_METHOD, REQUEST_PROTOCOL, REQUEST_FILENAME,
			REQUEST_HEADERS, REQUEST_HEADERS_NAMES, REQUEST_BODY,
			REQUEST_COOKIES, REQUEST_COOKIES_NAMES, RESPONSE_STATUS,
			RESPONSE_PROTOCOL, RESPONSE_HEADERS, RESPONSE_HEADERS_NAMES,
			RESPONSE_BODY, GEO, HIGHEST_SEVERITY, LOWEST_SEVERITY,

			PERF_REQ_START, PERF_REQ_PROC, PERF_COMBINED, PERF_PHASE1,
			PERF_PHASE2, PERF_PHASE3, PERF_PHASE4, PERF_PHASE5, PERF_SREAD,
			PERF_SWRITE, PERF_LOGGING, PERF_GC };

	/* An array of all variables which reference collections */
	public final static String[] COLLECTIONS = { ARGS, ARGS_NAMES,
			REQUEST_HEADERS, REQUEST_HEADERS_NAMES, REQUEST_COOKIES,
			REQUEST_COOKIES_NAMES, SESSION, TX, RESPONSE_HEADERS,
			RESPONSE_HEADERS_NAMES, GEO };

	public final static String[] SEVERITIES = { "EMERGENCY", "ALERT",
			"CRITICAL", "ERROR", "WARNING", "NOTICE", "INFO", "DEBUG",
			"SEVERITY-8", "SEVERITY-9", "SEVERITY-10", "SEVERITY-11",
			"SEVERITY-12", "SEVERITY-13", "SEVERITY-14", "SEVERITY_NOT_SET",
			"UNKNOWN" };

	public final static int SEVERITY_NOT_SET = 15;
	public final static int SEVERITY_UNKNOWN = 16;

	/**
	 * This method can be used to determine whether a given variable references
	 * a ModSecurity collection or not.
	 * 
	 * @param var
	 *            The variable (name) to check.
	 * @return <code>true</code>, if the name references a collection.
	 */
	public boolean isCollection(String var) {
		for (String col : COLLECTIONS) {
			if (col.equals(var))
				return true;
		}

		return false;
	}

	public static int getSeverity(String txt) {
		for (int i = 0; i < SEVERITIES.length; i++)
			if (SEVERITIES[i].equalsIgnoreCase(txt))
				return i;

		return SEVERITY_UNKNOWN;
	}

	public static String getSeverity(int level) {
		if (level >= 0 && level < SEVERITIES.length)
			return SEVERITIES[level];

		return SEVERITIES[SEVERITY_UNKNOWN];
	}

	public static boolean isCaseSensitive(String variable) {

		if (variable.startsWith(ModSecurity.REQUEST_HEADERS + ":"))
			return false;

		if (variable.startsWith("&" + ModSecurity.REQUEST_HEADERS + ":"))
			return false;

		if (variable.startsWith(ModSecurity.RESPONSE_HEADERS + ":"))
			return false;

		if (variable.startsWith("&" + ModSecurity.RESPONSE_HEADERS + ":"))
			return false;

		return true;
	}
}