/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit;

import org.jwall.audit.Event;
import org.jwall.audit.EventType;

import stream.data.DataImpl;

public class AuditData extends DataImpl implements Event {

	/** The unique class ID */
	private static final long serialVersionUID = 3092385126433800772L;
	
	public final static String TIMESTAMP_KEY = "@timestamp";
	
	
	public AuditData(){
		put( TIMESTAMP_KEY, new Long( System.currentTimeMillis() ) );
	}
	
	public AuditData( AuditEvent evt ){
		put( TIMESTAMP_KEY, evt.getDate().getTime() );
		
		for( int i = 0; i < ModSecurity.SECTIONS.length(); i++ ){
			String sect = evt.getSection(i);
			if( sect != null )
				put( "section:" + ModSecurity.SECTIONS.charAt( i ), sect );
		}
	}
	

	@Override
	public Long getTimestamp() {
		return (Long) super.get( TIMESTAMP_KEY );
	}


	@Override
	public final EventType getType() {
		return EventType.AUDIT;
	}


	@Override
	public String get(String variableName) {
		
		if( containsKey( variableName ) )
			return super.get(variableName) + "";
		
		return null;
	}


	@Override
	public void set(String variable, String value) {
		put( variable, value );
	}
}
