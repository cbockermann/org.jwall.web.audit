/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author chris
 * 
 */
public class IronBeeVariables {

	static Logger log = LoggerFactory.getLogger(IronBeeVariables.class);
	final static Map<String, String> aliases = new HashMap<String, String>();

	static {
		loadAliases();
	}

	protected static void loadAliases() {
		try {
			URL url = IronBeeVariables.class
					.getResource("/ironbee-variables.map");
			if (url != null) {
				Properties p = new Properties();
				p.load(url.openStream());
				for (Object key : p.keySet()) {
					aliases.put(key.toString(), p.getProperty(key.toString()));
				}
			}
			log.info("Loaded {} ironbee variable mappings.", aliases.size());
		} catch (Exception e) {
			log.error("Failed to load ironbee variable mappings: {}",
					e.getMessage());
			if (log.isTraceEnabled())
				e.printStackTrace();
		}
	}

	public final static void registerAlias(String jwallVariable,
			String ironBeeVariable) {
		aliases.put(jwallVariable, ironBeeVariable);
	}

	public final static String mapToIronBee(String variable) {
		if (aliases.containsKey(variable))
			return aliases.get(variable);

		int idx = variable.indexOf(":");
		if (idx > 0) {
			String prefix = variable.substring(0, idx);
			if (aliases.containsKey("@" + prefix)) {
				String mapped = variable.replaceFirst(prefix,
						aliases.get("@" + prefix));
				return mapped;
			}
		}

		return variable;
	}
}
