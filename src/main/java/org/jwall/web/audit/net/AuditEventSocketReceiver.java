/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.net;

import java.net.ServerSocket;
import java.net.Socket;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventListener;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.ModSecurity2AuditReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AuditEventSocketReceiver extends Thread {

	static Logger log = LoggerFactory.getLogger( AuditEventSocketReceiver.class );
	ServerSocket socket;
	AuditEventListener listener;

	public AuditEventSocketReceiver( int port ) throws Exception {
		socket = new ServerSocket( port );
	}

	public void run(){
		while( true ){
			try {
				Socket client = socket.accept();
				AuditEventReaderThread handler = new AuditEventReaderThread( client );
				log.info( "Starting new handler for incoming connection from {}:{}", client.getInetAddress().getHostAddress(), client.getPort() );
				handler.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}


	public class AuditEventReaderThread extends Thread {

		AuditEventReader reader;
		Socket socket;

		public AuditEventReaderThread( Socket sock ) throws Exception {
			this.socket = sock;
			reader = new ModSecurity2AuditReader( sock.getInputStream() );
		}

		public void run(){
			try {
				AuditEvent evt = reader.readNext();
				while( evt != null ){
					if( listener != null )
						listener.eventArrived( evt );
					evt = reader.readNext();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			log.info( "Handler for connection {}:{} exiting...", socket.getInetAddress(), socket.getPort() );
		}
	}
}
