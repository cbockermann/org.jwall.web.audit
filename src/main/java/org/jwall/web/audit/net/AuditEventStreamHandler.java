/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.net;

import java.io.EOFException;
import java.io.File;
import java.io.InputStream;
import java.net.Socket;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuditEventStreamHandler extends Thread implements AuditEventListener {

	static Logger log = LoggerFactory.getLogger( AuditEventStreamHandler.class );
	Long start;
	Integer count = 0;
	Socket socket;
	final static DecimalFormat fmt = new DecimalFormat( "0.00" );
	SyslogReceiverThread parent;
	boolean gzip = false;
	ArrayList<AuditEventListener> listener = new ArrayList<AuditEventListener>();
	boolean running = true;
	boolean base64 = false;
	
	public AuditEventStreamHandler( SyslogReceiverThread parent, Socket socket, boolean gzip, boolean base64 ) throws Exception {
		this.parent = parent;
		this.socket = socket;
		this.gzip = gzip;

		File outFile = new File( File.separator + "tmp" + File.separator + socket.getInetAddress().getHostAddress() + "-" + socket.getPort() + "-audit.log" );
		log.debug( "Writing to {}", outFile );
		setDaemon( true );
	}

	
	public void addListener( AuditEventListener l ){
		if( ! listener.contains( l ) ) 
			listener.add(l);
	}
	

	/**
	 * @see java.lang.Thread#run()
	 */
	public void run(){

		try {

			log.info( "Starting event-stream from connection {}", socket.getInetAddress() );
			start = System.currentTimeMillis();
			InputStream in = socket.getInputStream();
			SyslogAuditEventStream reader = new SyslogAuditEventStream( in, this );
			AuditEvent evt = reader.readNext();
			while( running && evt != null && !socket.isClosed() ){
				eventArrived( evt );
				evt = reader.readNext();
			}
			log.info( "Connection closed." );

		} catch (EOFException eof){
			log.info( "Connection closed." );
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if( parent != null )
			parent.handlerFinished( this );
	}


	@Override
	public void eventArrived(AuditEvent evt) {
		try {
			
			for( AuditEventListener l : listener ){
				//log.info( "Notifying listener " + l );
				l.eventArrived( evt );
			}
			
			count++;
			
			if( count % 100 == 0 ){
				Long time = (System.currentTimeMillis() - start) ;
				log.info( count +" events received in " + ((int)(time/1000)) + " seconds (" + fmt.format( ((1000*count.doubleValue() ) / time.doubleValue())  ) + " events/second)" );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Override
	public void eventsArrived(Collection<AuditEvent> events) {
		for( AuditEvent e : events )
			eventArrived( e );
	}
	
	public void close(){
		try {
			log.info( "Closing connection {}:{}", socket.getInetAddress(), socket.getPort() );
			socket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		running = false;
		this.interrupt();
	}
}