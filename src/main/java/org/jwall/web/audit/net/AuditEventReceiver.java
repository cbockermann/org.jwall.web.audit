/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.net;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

import org.jwall.web.audit.io.InputHandler;

public class AuditEventReceiver
{
    
    /**
     * @param args
     */
    public static void main(String[] args)
        throws Exception
    {
        String serv = "127.0.0.1";
        int port = 7901;
        
        
        ServerSocket socket = new ServerSocket(port, 10, InetAddress.getByName(serv) );

        while( true ){
            System.out.println( "EventReceiver ready, waiting for incoming connections!" );
            Socket sock = socket.accept();
            System.out.println( "New connection!" );
            InputHandler handler = new InputHandler( sock );
            handler.start();
        }
    }
}
