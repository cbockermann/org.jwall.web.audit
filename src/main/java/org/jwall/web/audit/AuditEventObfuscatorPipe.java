/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit;

import java.io.IOException;
import java.util.Collection;

import org.jwall.web.audit.util.Obfuscator;

public class AuditEventObfuscatorPipe
extends AuditEventObfuscatorChain
{
    AuditEventListener listener;
    
    
    public AuditEventObfuscatorPipe( AuditEventListener out ){
        this.listener = out;
    }


    public void addObfuscator( Obfuscator ob ){
        obfuscators.add( ob );
    }


    public void eventArrived(AuditEvent evt)
    {
        try {
            AuditEvent obfuscated = obfuscate( evt );
            listener.eventArrived( obfuscated );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void eventsArrived(Collection<AuditEvent> events)
    {
        for( AuditEvent evt : events )
            eventArrived( evt );
    }
}
