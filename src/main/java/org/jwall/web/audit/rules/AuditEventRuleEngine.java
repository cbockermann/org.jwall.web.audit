/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.rules;

import java.util.Map;

import org.jwall.audit.rules.EventRule;
import org.jwall.audit.rules.EventRuleEngine;
import org.jwall.web.audit.AuditEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <p>
 * A rule engine for handling audit-event data.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class AuditEventRuleEngine 
	extends EventRuleEngine<AuditEvent> 
{
	static Logger log = LoggerFactory.getLogger( AuditEventRuleEngine.class );

	@Override
	protected void execute(EventRule<AuditEvent> rule, AuditEvent event, Map<String,Object> context ) {
		if( rule instanceof AuditEventRule ){
			AuditEventRule r = (AuditEventRule) rule;
			for( EventAction act :  r.getActions() ){
				log.trace( "Executing rule-action {}", act );
				try {
					act.execute( context, event );
				} catch (Exception e) {
					log.error( "Failed to execute action {} for event {}", act, event );
					log.error( "   Error is: {}", e.getMessage() );
					if( log.isDebugEnabled() )
						e.printStackTrace();
				}
			}
		}
	}
}
