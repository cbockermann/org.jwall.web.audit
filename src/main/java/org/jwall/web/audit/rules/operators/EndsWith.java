/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.rules.operators;

import org.jwall.web.audit.SyntaxException;
import org.jwall.web.audit.filter.Operator;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * <p>
 * This condition checks for equality.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
@XStreamAlias("endsWith")
public class EndsWith
    extends AbstractCondition
{

    /** The unique class ID */
    private static final long serialVersionUID = 1323232629583570258L;

    public EndsWith(String variable, String value)
        throws SyntaxException
    {
        super(variable, value);
    }
    
    
    /**
     * @see org.jwall.web.audit.rules.operators.AbstractCondition#getOperator()
     */
    public String getOperator(){
        return Operator.EndsWith.toString();
    }

    
    /**
     * @see org.jwall.web.audit.rules.Condition#matches(java.lang.String, java.lang.String)
     */
	@Override
	public boolean matches(String pattern, String input) {
		return pattern != null && input != null && input.endsWith( pattern );
	}
}