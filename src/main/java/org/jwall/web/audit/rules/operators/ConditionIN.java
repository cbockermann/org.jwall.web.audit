/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.rules.operators;

import org.jwall.web.audit.SyntaxException;
import org.jwall.web.audit.filter.Operator;


/**
 * <p>
 * This class implements a condition called <code>@in</code>, which basically matches,
 * if one of a set of SX operators matches.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class ConditionIN extends ConditionSX {

	/** The unique class ID */
	private static final long serialVersionUID = 2437965899355955280L;

	public ConditionIN(String var, String exp) throws SyntaxException {
		super(var, exp);
	}

	/**
	 * @see org.jwall.web.audit.rules.operators.ConditionSX#getOperator()
	 */
	@Override
	public String getOperator() {
		return Operator.IN.toString();
	}

	/**
	 * @see org.jwall.web.audit.rules.operators.ConditionSX#matches(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean matches(String pattern, String input) {
		if( pattern.indexOf( "," ) >= 0 ){
			String[] patterns = pattern.split( "," );
			for( String p : patterns ){
				if( super.matches( p, input ) )
					return true;
			}
			return false;
		} else
			return super.matches(pattern, input);
	}
}