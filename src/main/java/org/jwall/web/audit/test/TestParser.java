/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.test;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.jwall.web.audit.filter.FilterCompiler;
import org.jwall.web.audit.filter.FilterExpression;

/**
 * <p>
 * This class parses a string line-by-line, reading a filter-expression from each line.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class TestParser {

	public static List<FilterExpression> parse( String str ) throws Exception {
		List<FilterExpression> list = new ArrayList<FilterExpression>();
		
		BufferedReader r = new BufferedReader( new StringReader( str ) );
		String line = r.readLine();
		while( line != null ){
			if( ! line.startsWith( "#" ) && !"".equals( line.trim() ) )
				list.add( FilterCompiler.parse( line.trim() ) );
			line = r.readLine();
		}
		r.close();
		return list;
	}
}
