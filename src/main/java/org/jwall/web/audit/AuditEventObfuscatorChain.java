/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.jwall.web.audit.util.Obfuscator;

/**
 * <p>
 * This class implements a chain of obfuscators. Obfuscators are small snipplets which may
 * replace/anonymize parts of an audit event.
 * </p>
 * @author Christian Bockermann &lt;chris@jwall.org;&gt;
 *
 */
public class AuditEventObfuscatorChain
{
    /** The list of obfuscators which shall be executed on the event */
    protected List<Obfuscator> obfuscators = null;



    /**
     * 
     * Create a new obfuscator chain.
     * 
     */
    public AuditEventObfuscatorChain(){

        obfuscators = new LinkedList<Obfuscator>();
    }



    /**
     * This adds a new obfuscator to the list of this chain.
     * 
     * @param ob
     */
    public void addObfuscator( Obfuscator ob ){
        obfuscators.add( ob );
    }


    /**
     * Checks whether the given obfuscator needs to process the section with the
     * specified ID.
     * 
     * @param obfuscator
     * @param id
     * @return
     */
    public boolean obfuscatesSection( Obfuscator obfuscator, Integer id ){
        return obfuscator.getSections().contains( id );
    }



    public AuditEvent obfuscate( AuditEvent evt ) throws IOException {

        String[] sections = evt.getRawData();

        for( int i = 0; i < sections.length; i++ ){

            for( Obfuscator ob : obfuscators ){
                if( obfuscatesSection( ob, i ) )
                    sections[i] = ob.obfuscate( i, sections[i] );
            }

        }

        for( Obfuscator ob : obfuscators )
            ob.done();

        return new ModSecurityAuditEvent( sections, AuditEventType.ModSecurity2 );
    }
}
