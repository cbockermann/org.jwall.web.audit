/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit;

import java.io.Serializable;

/**
 * This enumeration enlists the event formats supported by the storage
 * engine. Currently only support for ModSecurity2 is implemented. 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public enum AuditEventType
	implements Serializable
{

	ModSecurity1 ( "ModSecurity-1.x" ),
	ModSecurity2 ( "ModSecurity-2.x" ),
	IronBee ( "IronBee" );
	
	
	private String name;
	
	AuditEventType( String name ){
		this.name = name;
	}
	
	
	public String toString(){
		return name;
	}
}