/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.util;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.ProcessContext;
import stream.annotations.Parameter;

/**
 * <p>
 * This simple processor adds a timestamp (current time in milliseconds) to all
 * processed data items.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class Timestamp extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(Timestamp.class);
	SimpleDateFormat dateFormat = null;
	String key = "@timestamp";
	String format = null;
	String from = null;

	public Timestamp() {
	}

	public Timestamp(String key, String format, String from) {
		setKey(key);
		setFormat(format);
		setFrom(from);
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	@Parameter
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the format
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * @param format
	 *            the format to set
	 */
	public void setFormat(String format) {
		this.format = format;
	}

	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * @param from
	 *            the from to set
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * 
	 */
	@Override
	public Data process(Data data) {

		if (data != null && key != null) {

			Serializable from = data.get(getFrom());
			if (dateFormat != null && from != null) {
				try {
					Date date = dateFormat.parse(from.toString());
					data.put(key, date.getTime());
				} catch (Exception e) {
					log.error(
							"Failed to parse timestamp from '{}', expected format is '{}'",
							from, format);
				}
			} else {
				data.put(key, System.currentTimeMillis());
			}
		}

		return data;
	}

	/**
	 * 
	 */
	@Override
	public void init(ProcessContext ctx) throws Exception {
		super.init(ctx);

		if (getFormat() != null && getFrom() != null) {
			dateFormat = new SimpleDateFormat(getFormat());
		}
	}
}
