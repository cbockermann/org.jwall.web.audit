/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.util;

import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jwall.web.audit.AuditEventDispatcher;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.AuditFormat;


/**
 * 
 * This is a simple server which provides a few features for controlling a set of
 * rfi-collector threads. Each thread is reading a single log-source (audit-event
 * or access-log) and will process it incrementally.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class RFICollectorServer
extends UnicastRemoteObject
implements RFICollectorService
{
    private static final long serialVersionUID = -1459208475221129186L;

    public final static int REGISTRY_PORT = 1099;

    /** The registry where this server registers itself */
    static Registry registry;

    /** A logger for this class */
    static Logger log = LoggerFactory.getLogger( "RFICollectorServer" );

    /** This is a map of dispatchers, allowing for several analyzer to listen on the same file */
    private HashMap<String,AuditEventDispatcher> sources;

    
    /**
     * 
     * @throws RemoteException
     */
    public RFICollectorServer() throws RemoteException {
        super();

        sources = new HashMap<String,AuditEventDispatcher>();
    }


    /**
     * @see org.jwall.web.audit.util.RFICollectorService#registerCollector(java.lang.String, java.util.Properties)
     */
    public void registerCollector( String file, Properties p ) throws RemoteException {


        AuditEventDispatcher source = null;

        // if no reader is listening non the file, we need to create one
        //
        if( ! sources.containsKey(file) ){

            try {

                boolean tail = "true".equals( p.getProperty( RFICollector.PROPERTY_TAIL ) );
                AuditEventReader reader = AuditFormat.createReader( file , tail );
                AuditEventDispatcher evtSource = new AuditEventDispatcher( reader );
                sources.put( file, evtSource );
                
            } catch (Exception e) {
                throw new RemoteException( e.getMessage() );
            }
        }
        source = sources.get( file );


        File outputdir = new File( "/tmp/rfi-collector" );
        outputdir.mkdirs();

        try {
            RFICollector collector = new RFICollector( null, p );
            source.addAuditEventListener( collector );
        } catch (Exception e) {
            throw new RemoteException( e.getMessage() );
        }

        if( !source.isAlive() && source.getAuditEventListeners().size() > 0 )
            source.start();
        else
            sources.remove( file );
    }


    /**
     * @see org.jwall.web.audit.util.RFICollectorService#unregisterCollector(java.lang.String)
     */
    public void unregisterCollector( String file ) throws RemoteException {

        synchronized( sources ){
            if( sources.containsKey( file ) ){

                AuditEventDispatcher d = sources.get( file );
                d.removeAuditEventListeners();

                sources.remove( file );

                try {
                    d.close();
                } catch ( IOException ie) {
                    throw new RemoteException( ie.getMessage() );
                }
            } else {
                
                log.info( "No collector listening on " + file );
                
            }
        }
        
    }


    /**
     * @see org.jwall.web.audit.util.RFICollectorService#listCollectors()
     */
    public List<String> listCollectors() throws RemoteException {
        List<String> list = new LinkedList<String>();

        for( String file : sources.keySet() )
            list.add( file + " (" + sources.get( file ).getNumberOfEvents() + " events)" );

        return list;
    }


    public static void initRegistry() throws Exception {

        try {
            registry = LocateRegistry.getRegistry( "127.0.0.1", REGISTRY_PORT );
            log.debug("Found registry: " + registry );
        } catch (Exception e) {
            registry = null;
        }

        if( registry == null ){
            log.debug("Creating new registry...");
            registry = LocateRegistry.createRegistry( REGISTRY_PORT );

            registry = LocateRegistry.getRegistry( REGISTRY_PORT );
        }
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {
        try {

            System.setProperty("java.rmi.hostname", "127.0.0.1");

            log.info( "Initializing registry..." );
            initRegistry();

            log.info( "Creating collector server..." );
            RFICollectorServer server = new RFICollectorServer();

            log.info( "Registering collector server to registry..." );
            registry.rebind( RFICollectorService.SERVICE_NAME, server );

        } catch (Exception e) {
            log.error( e.getMessage() );
            e.printStackTrace();
        }
    }
}
