/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.util;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Properties;


/**
 * 
 * This interface defines all methods which are neccessary for creating a new collector,
 * exploring the list of available listeners and stopping existing ones.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public interface RFICollectorService
    extends Remote
{
    public final static String SERVICE_NAME = "RFICollectorService";
    
    
    /**
     * This method registers a new collector instance which tracks the given
     * log file.
     * 
     * @param file
     * @param props
     * @throws RemoteException In case an error occurs such as the file does not
     *                         exist or the file-format is not supported. Since
     *                         currently only one collector can listen on a file,
     *                         an exception is also thrown if a new collector is
     *                         registered on a already observed file. 
     */
    public void registerCollector( String file, Properties props ) throws RemoteException;
    
    
    /**
     * This method stops the collector listening on the given file.
     * 
     * @param file The file on which the collector listens.
     * @throws RemoteException In case an error occurs.
     */
    public void unregisterCollector( String file ) throws RemoteException;
    
    
    /**
     * This method returns a list of all currently running listeners.
     * 
     * @return A list of textual descriptions of all listeners.
     * @throws RemoteException In case an error occurs.
     */
    public List<String> listCollectors() throws RemoteException;
}
