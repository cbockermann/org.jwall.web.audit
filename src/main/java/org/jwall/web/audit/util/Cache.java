/*
 *  Copyright (C) 2007-2014 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.util;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Cache<V> {

	int size;
	Map<String,V> data = new HashMap<String,V>();
	LinkedList<String> keys = new LinkedList<String>();
	long hits = 0L;
	long misses = 0L;
	
	public Cache( int size ){
		this.size = size;
		this.keys = new LinkedList<String>();
	}
	
	
	public void put( String key, V object ){
		data.put( key, object );
		keys.add( key );
		if( keys.size() > size )
			prune();
	}
	
	public boolean containsKey( String key ){
		boolean b = data.containsKey( key );
		if( b )
			hits++;
		else
			misses++;
		return b;
	}
	
	public V get( String key ){
		V dat = data.get( key );
		return dat;
	}
	
	public void prune(){
		while( keys.size() > size ){
			String key = keys.removeFirst();
			data.remove( key );
		}
	}
	
	public int size(){
		return keys.size();
	}
	
	public int maximumSize(){
		return size;
	}
	
	
	public boolean isFull(){
		return keys.size() >= size;
	}
	
	
	public Long getHits(){
		return hits;
	}
	
	public Long getMisses(){
		return misses;
	}
	
	
	public Double getUse(){
		return ( new Double( keys.size() ) / new Double( size ) );
	}
}